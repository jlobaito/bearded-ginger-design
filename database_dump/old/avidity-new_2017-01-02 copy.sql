# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: avidity-new
# Generation Time: 2017-01-03 05:50:15 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table craft_assetfiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assetfiles`;

CREATE TABLE `craft_assetfiles` (
  `id` int(11) NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kind` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `size` bigint(20) unsigned DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetfiles_filename_folderId_unq_idx` (`filename`,`folderId`),
  KEY `craft_assetfiles_sourceId_fk` (`sourceId`),
  KEY `craft_assetfiles_folderId_fk` (`folderId`),
  CONSTRAINT `craft_assetfiles_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `craft_assetfolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_assetfiles_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_assetfiles_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_assetfiles` WRITE;
/*!40000 ALTER TABLE `craft_assetfiles` DISABLE KEYS */;

INSERT INTO `craft_assetfiles` (`id`, `sourceId`, `folderId`, `filename`, `kind`, `width`, `height`, `size`, `dateModified`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(11,1,1,'Screen-Shot-2016-12-20-at-12.31.37-PM.png','image',374,444,24283,'2016-12-20 22:21:43','2016-12-20 22:21:43','2016-12-20 22:21:43','ec7c9570-ddc2-4a1e-b35a-1903782b2053'),
	(12,1,1,'Screen-Shot-2016-12-20-at-9.42.04-AM.png','image',797,551,131099,'2016-12-20 22:21:44','2016-12-20 22:21:44','2016-12-20 22:21:44','67c26b69-3394-49c2-b16d-eaf6cd30dbc2'),
	(13,1,1,'Screen-Shot-2016-12-19-at-4.40.10-PM.png','image',633,295,50996,'2016-12-20 22:21:44','2016-12-20 22:21:44','2016-12-20 22:21:44','ddcb2467-0102-42d0-ade4-3d660f9e1dbd');

/*!40000 ALTER TABLE `craft_assetfiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_assetfolders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assetfolders`;

CREATE TABLE `craft_assetfolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetfolders_name_parentId_sourceId_unq_idx` (`name`,`parentId`,`sourceId`),
  KEY `craft_assetfolders_parentId_fk` (`parentId`),
  KEY `craft_assetfolders_sourceId_fk` (`sourceId`),
  CONSTRAINT `craft_assetfolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `craft_assetfolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_assetfolders_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_assetfolders` WRITE;
/*!40000 ALTER TABLE `craft_assetfolders` DISABLE KEYS */;

INSERT INTO `craft_assetfolders` (`id`, `parentId`, `sourceId`, `name`, `path`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,1,'Media','','2016-12-20 22:19:47','2016-12-20 22:19:47','cbdce5f8-b1c2-42e7-aa3f-6ed4267cbfa7'),
	(2,NULL,NULL,'Temporary source',NULL,'2016-12-20 22:20:37','2016-12-20 22:20:37','9e8765a5-9bf4-4e98-ab16-6d9a9505c3a7'),
	(3,2,NULL,'user_1',NULL,'2016-12-20 22:20:37','2016-12-20 22:20:37','fde947d4-416e-4cb7-87de-3e3eed61ded1'),
	(4,3,NULL,'field_28','field_28/','2016-12-20 22:20:37','2016-12-20 22:20:37','7392485a-a4e9-4bb8-90eb-8cb70643833e');

/*!40000 ALTER TABLE `craft_assetfolders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_assetindexdata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assetindexdata`;

CREATE TABLE `craft_assetindexdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sourceId` int(10) NOT NULL,
  `offset` int(10) NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recordId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetindexdata_sessionId_sourceId_offset_unq_idx` (`sessionId`,`sourceId`,`offset`),
  KEY `craft_assetindexdata_sourceId_fk` (`sourceId`),
  CONSTRAINT `craft_assetindexdata_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_assetsources` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_assetsources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assetsources`;

CREATE TABLE `craft_assetsources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assetsources_name_unq_idx` (`name`),
  UNIQUE KEY `craft_assetsources_handle_unq_idx` (`handle`),
  KEY `craft_assetsources_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_assetsources_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_assetsources` WRITE;
/*!40000 ALTER TABLE `craft_assetsources` DISABLE KEYS */;

INSERT INTO `craft_assetsources` (`id`, `name`, `handle`, `type`, `settings`, `sortOrder`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Media','media','Local','{\"path\":\"{basePath}resources\\/media\\/\",\"publicURLs\":\"1\",\"url\":\"\\/resources\\/media\\/\"}',1,54,'2016-12-20 22:19:47','2016-12-20 23:18:30','414779dc-2bf4-48e5-b1b9-b59f8849e6a6');

/*!40000 ALTER TABLE `craft_assetsources` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_assettransformindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assettransformindex`;

CREATE TABLE `craft_assettransformindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT NULL,
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_assettransformindex_sourceId_fileId_location_idx` (`sourceId`,`fileId`,`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_assettransformindex` WRITE;
/*!40000 ALTER TABLE `craft_assettransformindex` DISABLE KEYS */;

INSERT INTO `craft_assettransformindex` (`id`, `fileId`, `filename`, `format`, `location`, `sourceId`, `fileExists`, `inProgress`, `dateIndexed`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,13,'Screen-Shot-2016-12-19-at-4.40.10-PM.png',NULL,'_thumbnail',1,1,0,'2016-12-20 23:18:33','2016-12-20 23:18:33','2016-12-20 23:18:33','219e5a71-f496-474f-a496-9a4dcd22894c'),
	(2,11,'Screen-Shot-2016-12-20-at-12.31.37-PM.png',NULL,'_thumbnail',1,1,0,'2016-12-20 23:18:33','2016-12-20 23:18:33','2016-12-20 23:18:33','cd75a4c9-1c2a-42a0-b3ca-f0e1f07536c6'),
	(3,12,'Screen-Shot-2016-12-20-at-9.42.04-AM.png',NULL,'_thumbnail',1,1,0,'2016-12-20 23:18:33','2016-12-20 23:18:33','2016-12-20 23:18:33','24834187-f62e-4b7c-968a-477985d5466d'),
	(4,11,'Screen-Shot-2016-12-20-at-12.31.37-PM.png',NULL,'_brand',1,1,0,'2017-01-03 04:26:47','2017-01-03 04:26:47','2017-01-03 04:26:48','c9927d69-3eb2-4e7f-9d90-c37697a4d53f');

/*!40000 ALTER TABLE `craft_assettransformindex` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_assettransforms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assettransforms`;

CREATE TABLE `craft_assettransforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mode` enum('stretch','fit','crop') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'center-center',
  `height` int(10) DEFAULT NULL,
  `width` int(10) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quality` int(10) DEFAULT NULL,
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assettransforms_name_unq_idx` (`name`),
  UNIQUE KEY `craft_assettransforms_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_assettransforms` WRITE;
/*!40000 ALTER TABLE `craft_assettransforms` DISABLE KEYS */;

INSERT INTO `craft_assettransforms` (`id`, `name`, `handle`, `mode`, `position`, `height`, `width`, `format`, `quality`, `dimensionChangeTime`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Thumbnail','thumbnail','crop','center-center',300,300,NULL,NULL,'2016-12-20 22:25:21','2016-12-20 22:25:21','2016-12-20 22:25:21','bfe27551-1d3d-43d6-8d57-a89e6b63520e'),
	(2,'Brand','brand','crop','center-center',50,50,NULL,NULL,'2017-01-03 04:26:46','2017-01-03 04:26:46','2017-01-03 04:26:46','07562f0d-576b-43f9-b080-cf5a7bd47e8b');

/*!40000 ALTER TABLE `craft_assettransforms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_categories`;

CREATE TABLE `craft_categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_categories_groupId_fk` (`groupId`),
  CONSTRAINT `craft_categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_categories_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_categorygroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_categorygroups`;

CREATE TABLE `craft_categorygroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_categorygroups_name_unq_idx` (`name`),
  UNIQUE KEY `craft_categorygroups_handle_unq_idx` (`handle`),
  KEY `craft_categorygroups_structureId_fk` (`structureId`),
  KEY `craft_categorygroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_categorygroups_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_categorygroups_i18n`;

CREATE TABLE `craft_categorygroups_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_categorygroups_i18n_groupId_locale_unq_idx` (`groupId`,`locale`),
  KEY `craft_categorygroups_i18n_locale_fk` (`locale`),
  CONSTRAINT `craft_categorygroups_i18n_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_categorygroups_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_content`;

CREATE TABLE `craft_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_body` text COLLATE utf8_unicode_ci,
  `field_flickr` text COLLATE utf8_unicode_ci,
  `field_facebook` text COLLATE utf8_unicode_ci,
  `field_twitter` text COLLATE utf8_unicode_ci,
  `field_google` text COLLATE utf8_unicode_ci,
  `field_pinterest` text COLLATE utf8_unicode_ci,
  `field_linkedin` text COLLATE utf8_unicode_ci,
  `field_primaryTelephone` text COLLATE utf8_unicode_ci,
  `field_gaCode` text COLLATE utf8_unicode_ci,
  `field_primaryEmail` text COLLATE utf8_unicode_ci,
  `field_defaultSeo` text COLLATE utf8_unicode_ci,
  `field_instagram` text COLLATE utf8_unicode_ci,
  `field_pageDescription` text COLLATE utf8_unicode_ci,
  `field_street` text COLLATE utf8_unicode_ci,
  `field_city` text COLLATE utf8_unicode_ci,
  `field_copyLeft` text COLLATE utf8_unicode_ci,
  `field_copyRight` text COLLATE utf8_unicode_ci,
  `field_quote` text COLLATE utf8_unicode_ci,
  `field_byline` text COLLATE utf8_unicode_ci,
  `field_ctaText` text COLLATE utf8_unicode_ci,
  `field_ctaLinkExternal` text COLLATE utf8_unicode_ci,
  `field_contentGridHeadline` text COLLATE utf8_unicode_ci,
  `field_contentGridContent` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_content_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `craft_content_title_idx` (`title`),
  KEY `craft_content_locale_fk` (`locale`),
  CONSTRAINT `craft_content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_content_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_content` WRITE;
/*!40000 ALTER TABLE `craft_content` DISABLE KEYS */;

INSERT INTO `craft_content` (`id`, `elementId`, `locale`, `title`, `field_body`, `field_flickr`, `field_facebook`, `field_twitter`, `field_google`, `field_pinterest`, `field_linkedin`, `field_primaryTelephone`, `field_gaCode`, `field_primaryEmail`, `field_defaultSeo`, `field_instagram`, `field_pageDescription`, `field_street`, `field_city`, `field_copyLeft`, `field_copyRight`, `field_quote`, `field_byline`, `field_ctaText`, `field_ctaLinkExternal`, `field_contentGridHeadline`, `field_contentGridContent`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 19:47:26','2016-12-20 19:47:26','dd9defae-6b20-49be-8ad7-0fd37929d8c8'),
	(2,2,'en_us','Welcome to Local.craft.dev!','<p>It’s true, this site doesn’t have a whole lot of content yet, but don’t worry. Our web developers have just installed the CMS, and they’re setting things up for the content editors this very moment. Soon Local.craft.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.</p>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 19:47:28','2017-01-03 05:32:15','059eb738-9627-40cd-9301-e5a01c3040d7'),
	(3,3,'en_us','We just installed Craft!','<p>Craft is the CMS that’s powering Local.craft.dev. It’s beautiful, powerful, flexible, and easy-to-use, and it’s made by Pixel &amp; Tonic. We can’t wait to dive in and see what it’s capable of!</p><!--pagebreak--><p>This is even more captivating content, which you couldn’t see on the News index page because it was entered after a Page Break, and the News index template only likes to show the content on the first page.</p><p>Craft: a nice alternative to Word, if you’re making a website.</p>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 19:47:28','2016-12-20 19:47:28','403b195d-fd20-4d83-b10c-df6e58e1fa29'),
	(4,4,'en_us',NULL,NULL,'','test','','','','',NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 19:48:11','2016-12-20 19:57:41','2151d3c7-7491-441e-ba44-c2b8c7df3128'),
	(5,5,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','test','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 19:53:48','2016-12-20 19:55:51','6d8dff9f-9b99-4150-af1f-f3a4a1434397'),
	(6,11,'en_us','Screen Shot 2016 12 20 At 12 31 37 Pm',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 22:21:43','2016-12-20 22:21:43','e7dc57bb-c680-4cd8-93fd-48e0a6262304'),
	(7,12,'en_us','Screen Shot 2016 12 20 At 9 42 04 Am',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 22:21:44','2016-12-20 22:21:44','f5dc9fbf-2a6b-4f1a-bdbd-e3f44d635edc'),
	(8,13,'en_us','Screen Shot 2016 12 19 At 4 40 10 Pm',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 22:21:44','2016-12-20 22:21:44','ce6a19a0-72fe-4c3c-a9fe-d7442e4dba84'),
	(10,15,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.</p>','<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.</p>',NULL,NULL,NULL,NULL,NULL,NULL,'2017-01-03 04:01:34','2017-01-03 04:01:34','19dcce3d-90ce-4105-a1a6-bb5052da8fc1'),
	(11,16,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.','test byline',NULL,NULL,NULL,NULL,'2017-01-03 04:19:50','2017-01-03 04:21:50','6654f82c-dae8-40aa-a678-8187daef29bb'),
	(22,27,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-01-03 04:47:34','2017-01-03 04:47:34','a5f0dab2-6310-4cec-915a-33882731de4b'),
	(23,28,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'This is a link',NULL,NULL,NULL,'2017-01-03 05:02:50','2017-01-03 05:02:50','5af81492-536b-4047-8b1e-c079eb2a1356'),
	(24,29,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-01-03 05:32:15','2017-01-03 05:32:15','6ccf1456-48b6-4ecf-bab3-04629f8092ab'),
	(25,30,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Test 1','<p>;lkajdsf ksdf;kal;dfja sdl;kjfal;ksdjf </p>','2017-01-03 05:32:15','2017-01-03 05:32:15','6f9cd65e-ff1a-4259-98bc-465c5679b32e'),
	(26,31,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Test 2','<p>;lkajdsf alkdsjfl;kajsd fkl;</p>','2017-01-03 05:32:15','2017-01-03 05:32:15','f5b3d8ff-1efa-447e-8c59-d996c7c53556');

/*!40000 ALTER TABLE `craft_content` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_deprecationerrors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_deprecationerrors`;

CREATE TABLE `craft_deprecationerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fingerprint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `line` smallint(6) unsigned NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `templateLine` smallint(6) unsigned DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `traces` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_elementindexsettings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_elementindexsettings`;

CREATE TABLE `craft_elementindexsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_elementindexsettings_type_unq_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_elements`;

CREATE TABLE `craft_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_elements_type_idx` (`type`),
  KEY `craft_elements_enabled_idx` (`enabled`),
  KEY `craft_elements_archived_dateCreated_idx` (`archived`,`dateCreated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_elements` WRITE;
/*!40000 ALTER TABLE `craft_elements` DISABLE KEYS */;

INSERT INTO `craft_elements` (`id`, `type`, `enabled`, `archived`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'User',1,0,'2016-12-20 19:47:26','2016-12-20 19:47:26','b2a72b10-5615-4fcf-b634-82e0872980ae'),
	(2,'Entry',1,0,'2016-12-20 19:47:28','2017-01-03 05:32:15','2db1a6c1-cbd9-4b73-a1af-b040d83519b4'),
	(3,'Entry',1,0,'2016-12-20 19:47:28','2016-12-20 19:47:28','a30b975f-a5ba-4554-a742-37f04b6ac8b5'),
	(4,'GlobalSet',1,0,'2016-12-20 19:48:11','2016-12-20 19:57:41','853273e6-f1d8-4d52-9f56-40c9b7482f1d'),
	(5,'GlobalSet',1,0,'2016-12-20 19:53:48','2016-12-20 19:55:51','25b4e62e-9903-481c-89b0-e4117f13f434'),
	(6,'MatrixBlock',1,0,'2016-12-20 21:17:19','2016-12-20 22:21:48','2c68403d-0dda-4ccb-b098-e0e787499009'),
	(7,'MatrixBlock',1,0,'2016-12-20 21:17:19','2016-12-20 22:21:48','504593cc-9028-4f20-af26-d91a58afded4'),
	(8,'MatrixBlock',1,0,'2016-12-20 21:52:55','2016-12-20 22:21:48','58313d54-1677-41ea-883f-b9dca36290cd'),
	(9,'MatrixBlock',1,0,'2016-12-20 21:53:39','2016-12-20 22:21:48','7318b27e-a3f4-4320-8194-113c855d5209'),
	(10,'MatrixBlock',1,0,'2016-12-20 22:18:51','2016-12-20 22:21:48','17c76b96-edb5-4409-8eac-5c0728e683c3'),
	(11,'Asset',1,0,'2016-12-20 22:21:43','2016-12-20 22:21:43','cf7ce9f4-170c-4e0d-8c91-5c5602244ae7'),
	(12,'Asset',1,0,'2016-12-20 22:21:44','2016-12-20 22:21:44','9c4f9fe2-27b0-472a-8bec-ef1193d9d585'),
	(13,'Asset',1,0,'2016-12-20 22:21:44','2016-12-20 22:21:44','68a06648-fb3e-4564-812e-7f064fc0d5b8'),
	(15,'Neo_Block',1,0,'2017-01-03 04:01:34','2017-01-03 04:01:34','564952f2-6237-459c-a0d9-fc4be8b5037c'),
	(16,'Neo_Block',1,0,'2017-01-03 04:19:50','2017-01-03 04:21:50','b63d2721-fa3b-4965-a034-6bd3a4f0c1ea'),
	(27,'Neo_Block',1,0,'2017-01-03 04:47:34','2017-01-03 04:47:34','f29f0526-dc96-46e0-a2d4-32634ec4d618'),
	(28,'Neo_Block',1,0,'2017-01-03 05:02:50','2017-01-03 05:02:50','d6cb3fca-5278-4c76-800b-42487a73ec4c'),
	(29,'Neo_Block',1,0,'2017-01-03 05:32:15','2017-01-03 05:32:15','f2436e0b-de31-4115-800c-7fce66b9e060'),
	(30,'Neo_Block',1,0,'2017-01-03 05:32:15','2017-01-03 05:32:15','e5e40af9-b0f0-4c13-a365-685cbb8ef494'),
	(31,'Neo_Block',1,0,'2017-01-03 05:32:15','2017-01-03 05:32:15','3c20be2e-d473-4196-a8b3-c8847b150341');

/*!40000 ALTER TABLE `craft_elements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_elements_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_elements_i18n`;

CREATE TABLE `craft_elements_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_elements_i18n_elementId_locale_unq_idx` (`elementId`,`locale`),
  UNIQUE KEY `craft_elements_i18n_uri_locale_unq_idx` (`uri`,`locale`),
  KEY `craft_elements_i18n_slug_locale_idx` (`slug`,`locale`),
  KEY `craft_elements_i18n_enabled_idx` (`enabled`),
  KEY `craft_elements_i18n_locale_fk` (`locale`),
  CONSTRAINT `craft_elements_i18n_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_elements_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_elements_i18n` WRITE;
/*!40000 ALTER TABLE `craft_elements_i18n` DISABLE KEYS */;

INSERT INTO `craft_elements_i18n` (`id`, `elementId`, `locale`, `slug`, `uri`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us','',NULL,1,'2016-12-20 19:47:26','2016-12-20 19:47:26','90c86e13-2ec5-451c-8e4c-13253d5a7c31'),
	(2,2,'en_us','homepage','__home__',1,'2016-12-20 19:47:28','2017-01-03 05:32:15','60896d95-d056-43bd-acbc-27a6298e5ab4'),
	(3,3,'en_us','we-just-installed-craft','news/2016/we-just-installed-craft',1,'2016-12-20 19:47:29','2016-12-20 19:47:29','69db4aed-bf71-4240-a12b-fd18f70a576f'),
	(4,4,'en_us','',NULL,1,'2016-12-20 19:48:11','2016-12-20 19:57:41','948dbf65-3342-45fc-9fe9-e41012489e30'),
	(5,5,'en_us','',NULL,1,'2016-12-20 19:53:48','2016-12-20 19:55:51','e0675fea-13b4-4944-95e7-54efa7f042f8'),
	(6,6,'en_us','',NULL,1,'2016-12-20 21:17:19','2016-12-20 22:21:48','a0901faf-450b-4f45-8e06-2f155f879f45'),
	(7,7,'en_us','',NULL,1,'2016-12-20 21:17:19','2016-12-20 22:21:48','b836e61f-f9a9-4911-b60d-e68f04f61cd6'),
	(8,8,'en_us','',NULL,1,'2016-12-20 21:52:55','2016-12-20 22:21:48','cf1ce6ef-19ef-45d8-a81e-8dd00ed6b000'),
	(9,9,'en_us','',NULL,1,'2016-12-20 21:53:39','2016-12-20 22:21:48','d7fb727e-2628-4d07-aaf7-7e62718c27d7'),
	(10,10,'en_us','',NULL,1,'2016-12-20 22:18:51','2016-12-20 22:21:48','de17cdb3-d26a-4533-81c4-612f84c0fc23'),
	(11,11,'en_us','screen-shot-2016-12-20-at-12-31-37-pm',NULL,1,'2016-12-20 22:21:43','2016-12-20 22:21:43','536768cd-4f3f-4bc4-a0bb-ab3a04ef669f'),
	(12,12,'en_us','screen-shot-2016-12-20-at-9-42-04-am',NULL,1,'2016-12-20 22:21:44','2016-12-20 22:21:44','c7c0a69c-f3b6-4b4f-8eab-29f8b42156b6'),
	(13,13,'en_us','screen-shot-2016-12-19-at-4-40-10-pm',NULL,1,'2016-12-20 22:21:44','2016-12-20 22:21:44','b74a0933-195d-4df9-9041-c260dc878906'),
	(15,15,'en_us','',NULL,1,'2017-01-03 04:01:34','2017-01-03 04:01:34','8ed68715-95b8-4b92-a347-b48cf1bf3cbe'),
	(16,16,'en_us','',NULL,1,'2017-01-03 04:19:50','2017-01-03 04:21:50','ec4fe947-5450-4a2d-a959-8347e7369da7'),
	(27,27,'en_us','',NULL,1,'2017-01-03 04:47:34','2017-01-03 04:47:34','a4a7ea0e-e5de-4275-a015-594b354a3e33'),
	(28,28,'en_us','',NULL,1,'2017-01-03 05:02:50','2017-01-03 05:02:50','a09f5795-f137-423e-8068-4bb0ee39eed6'),
	(29,29,'en_us','',NULL,1,'2017-01-03 05:32:15','2017-01-03 05:32:15','86c22e8b-c07d-4827-916f-83e416c26adb'),
	(30,30,'en_us','',NULL,1,'2017-01-03 05:32:15','2017-01-03 05:32:15','28526635-ebef-4770-a41a-daad5e05254e'),
	(31,31,'en_us','',NULL,1,'2017-01-03 05:32:15','2017-01-03 05:32:15','7d58f67d-4e75-4543-ac94-51a9b34141d4');

/*!40000 ALTER TABLE `craft_elements_i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_emailmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_emailmessages`;

CREATE TABLE `craft_emailmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` char(150) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_emailmessages_key_locale_unq_idx` (`key`,`locale`),
  KEY `craft_emailmessages_locale_fk` (`locale`),
  CONSTRAINT `craft_emailmessages_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_entries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entries`;

CREATE TABLE `craft_entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entries_sectionId_idx` (`sectionId`),
  KEY `craft_entries_typeId_idx` (`typeId`),
  KEY `craft_entries_postDate_idx` (`postDate`),
  KEY `craft_entries_expiryDate_idx` (`expiryDate`),
  KEY `craft_entries_authorId_fk` (`authorId`),
  CONSTRAINT `craft_entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entries_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `craft_entrytypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_entries` WRITE;
/*!40000 ALTER TABLE `craft_entries` DISABLE KEYS */;

INSERT INTO `craft_entries` (`id`, `sectionId`, `typeId`, `authorId`, `postDate`, `expiryDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,1,NULL,NULL,'2016-12-20 19:47:28',NULL,'2016-12-20 19:47:28','2017-01-03 05:32:15','c7c7113d-4e69-49d6-976e-9f8c383198c9'),
	(3,2,2,1,'2016-12-20 19:47:28',NULL,'2016-12-20 19:47:29','2016-12-20 19:47:29','de66bab2-77d9-4d13-93d9-19636159b713');

/*!40000 ALTER TABLE `craft_entries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_entrydrafts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entrydrafts`;

CREATE TABLE `craft_entrydrafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entrydrafts_entryId_locale_idx` (`entryId`,`locale`),
  KEY `craft_entrydrafts_sectionId_fk` (`sectionId`),
  KEY `craft_entrydrafts_creatorId_fk` (`creatorId`),
  KEY `craft_entrydrafts_locale_fk` (`locale`),
  CONSTRAINT `craft_entrydrafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entrydrafts_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `craft_entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entrydrafts_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_entrydrafts_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_entrytypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entrytypes`;

CREATE TABLE `craft_entrytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasTitleField` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Title',
  `titleFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_entrytypes_name_sectionId_unq_idx` (`name`,`sectionId`),
  UNIQUE KEY `craft_entrytypes_handle_sectionId_unq_idx` (`handle`,`sectionId`),
  KEY `craft_entrytypes_sectionId_fk` (`sectionId`),
  KEY `craft_entrytypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_entrytypes` WRITE;
/*!40000 ALTER TABLE `craft_entrytypes` DISABLE KEYS */;

INSERT INTO `craft_entrytypes` (`id`, `sectionId`, `fieldLayoutId`, `name`, `handle`, `hasTitleField`, `titleLabel`, `titleFormat`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,56,'Homepage','homepage',1,'Title',NULL,1,'2016-12-20 19:47:28','2017-01-03 03:49:42','e9043742-add2-489e-bbcc-22c11fa52d09'),
	(2,2,5,'News','news',1,'Title',NULL,1,'2016-12-20 19:47:28','2016-12-20 19:47:28','37ded7fa-6786-4a87-980e-09b27ad498e8'),
	(3,3,129,'Landing Page','landingPage',1,'Title',NULL,1,'2017-01-03 05:14:29','2017-01-03 05:14:29','f757f755-12b0-424b-8749-c00658e03816');

/*!40000 ALTER TABLE `craft_entrytypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_entryversions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entryversions`;

CREATE TABLE `craft_entryversions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `num` smallint(6) unsigned NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entryversions_entryId_locale_idx` (`entryId`,`locale`),
  KEY `craft_entryversions_sectionId_fk` (`sectionId`),
  KEY `craft_entryversions_creatorId_fk` (`creatorId`),
  KEY `craft_entryversions_locale_fk` (`locale`),
  CONSTRAINT `craft_entryversions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_entryversions_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `craft_entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entryversions_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_entryversions_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_entryversions` WRITE;
/*!40000 ALTER TABLE `craft_entryversions` DISABLE KEYS */;

INSERT INTO `craft_entryversions` (`id`, `entryId`, `sectionId`, `creatorId`, `locale`, `num`, `notes`, `data`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,2,1,1,'en_us',1,NULL,'{\"typeId\":\"1\",\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2016-12-20 19:47:28','2016-12-20 19:47:28','974f0055-acdb-42fe-97c6-270427f92c76'),
	(2,2,1,1,'en_us',2,NULL,'{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":\"1\",\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.craft.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\"}}','2016-12-20 19:47:28','2016-12-20 19:47:28','a6bf7293-4221-4959-9ae5-772dfa40d706'),
	(3,3,2,1,'en_us',1,NULL,'{\"typeId\":\"2\",\"authorId\":\"1\",\"title\":\"We just installed Craft!\",\"slug\":\"we-just-installed-craft\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}','2016-12-20 19:47:29','2016-12-20 19:47:29','f0b72076-0612-4b00-bbc8-7dd29fec4f52'),
	(4,2,1,1,'en_us',3,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Local.craft.dev will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\",\"16\":\"\",\"17\":\"test\"}}','2016-12-20 20:11:45','2016-12-20 20:11:45','6b996ebb-e326-4eeb-81fd-21a9814512f4'),
	(5,2,1,1,'en_us',4,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"new1\":{\"type\":\"contentBuilderContent\",\"enabled\":\"1\",\"fields\":{\"richText\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"new2\":{\"type\":\"twoColumn\",\"enabled\":\"1\",\"fields\":{\"contentLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"contentRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}}},\"16\":\"\"}}','2016-12-20 21:17:19','2016-12-20 21:17:19','d39cfa9d-ce89-4bae-8a32-6e8f4f8fc926'),
	(6,2,1,1,'en_us',5,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"6\":{\"type\":\"contentBuilderContent\",\"enabled\":\"1\",\"fields\":{\"fullContent\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"7\":{\"type\":\"twoColumn\",\"enabled\":\"1\",\"fields\":{\"contentLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"contentRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}}},\"16\":\"\"}}','2016-12-20 21:18:45','2016-12-20 21:18:45','01079caa-cdf4-43eb-9855-95bbf0e9cb3e'),
	(7,2,1,1,'en_us',6,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"7\":{\"type\":\"twoColumn\",\"enabled\":\"1\",\"fields\":{\"contentLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"contentRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"6\":{\"type\":\"contentBuilderContent\",\"enabled\":\"1\",\"fields\":{\"fullContent\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}}},\"16\":\"\"}}','2016-12-20 21:20:20','2016-12-20 21:20:20','82fdda5a-f79b-4562-8177-a6851dd3e499'),
	(8,2,1,1,'en_us',7,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"7\":{\"type\":\"twoColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"6\":{\"type\":\"fullCopy\",\"enabled\":\"1\",\"fields\":{\"copy\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"new1\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}}},\"16\":\"\"}}','2016-12-20 21:52:55','2016-12-20 21:52:55','21fb379a-4f0c-4b6b-a1e7-145668a54809'),
	(9,2,1,1,'en_us',8,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"7\":{\"type\":\"twoColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"6\":{\"type\":\"fullCopy\",\"enabled\":\"1\",\"fields\":{\"copy\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"8\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"new1\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p><strong><\\/strong>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}}},\"16\":\"\"}}','2016-12-20 21:53:39','2016-12-20 21:53:39','272f83b1-bc60-47cf-8c9b-933d0ffe8bf6'),
	(10,2,1,1,'en_us',9,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"7\":{\"type\":\"twoColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"6\":{\"type\":\"fullCopy\",\"enabled\":\"1\",\"fields\":{\"copy\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"8\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"9\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"new1\":{\"type\":\"photoGallery\",\"enabled\":\"1\"}},\"16\":\"\"}}','2016-12-20 22:18:51','2016-12-20 22:18:51','30e3323c-c183-4902-940d-8928d1fcea28'),
	(11,2,1,1,'en_us',10,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"7\":{\"type\":\"twoColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"6\":{\"type\":\"fullCopy\",\"enabled\":\"1\",\"fields\":{\"copy\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"8\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"9\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"10\":{\"type\":\"photoGallery\",\"enabled\":\"1\",\"fields\":{\"photos\":[\"13\",\"11\",\"12\"]}}},\"16\":\"\"}}','2016-12-20 22:21:47','2016-12-20 22:21:47','4142deb1-aca7-44f9-97cc-821f6e701d67'),
	(12,2,1,1,'en_us',11,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"7\":{\"type\":\"twoColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"6\":{\"type\":\"fullCopy\",\"enabled\":\"1\",\"fields\":{\"copy\":\"<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"8\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"9\":{\"type\":\"threeColumnCopy\",\"enabled\":\"1\",\"fields\":{\"copyLeft\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyMiddle\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\",\"copyRight\":\"<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.<\\/p>\"}},\"10\":{\"type\":\"photoGallery\",\"enabled\":\"1\",\"fields\":{\"photos\":[\"13\",\"11\",\"12\"]}}},\"16\":\"\"}}','2016-12-20 22:21:48','2016-12-20 22:21:48','9babc893-05bd-4451-ab2f-3392b4bc5b6b'),
	(13,2,1,1,'en_us',12,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"29\":{\"new0\":{\"modified\":\"1\",\"type\":\"textField\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"body\":\"<p>Test&nbsp;Test Test<\\/p>\"}}},\"16\":\"\"}}','2017-01-03 03:49:53','2017-01-03 03:49:53','3beb972a-d522-4b08-baaf-dae500e3bc92'),
	(14,2,1,1,'en_us',13,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"29\":{\"14\":{\"modified\":\"0\",\"type\":\"textField\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"body\":\"<p>Test&nbsp;Test Test<\\/p>\"}},\"new0\":{\"modified\":\"1\",\"type\":\"twoColumn\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"contentleft\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\",\"contentright\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\"}}},\"16\":\"\"}}','2017-01-03 04:01:34','2017-01-03 04:01:34','0923e049-38ad-4e42-8eda-1c2d6b212b8f'),
	(15,2,1,1,'en_us',14,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"29\":{\"15\":{\"modified\":\"0\",\"type\":\"twoColumn\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"copyLeft\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\",\"copyRight\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\"}}},\"16\":\"\"}}','2017-01-03 04:07:46','2017-01-03 04:07:46','d822a3ea-f6af-4e09-b74c-1f88884c7292'),
	(16,2,1,1,'en_us',15,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"29\":{\"15\":{\"modified\":\"0\",\"type\":\"twoColumn\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"copyLeft\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\",\"copyRight\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\"}},\"new0\":{\"modified\":\"1\",\"type\":\"quote\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"brand\":\"\",\"quote\":\"cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.\",\"byline\":\"test byline\"}}},\"16\":\"\"}}','2017-01-03 04:19:50','2017-01-03 04:19:50','b6ebcdfc-61bf-471a-a95e-4a94d0cdc949'),
	(17,2,1,1,'en_us',16,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"29\":{\"15\":{\"modified\":\"0\",\"type\":\"twoColumn\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"copyLeft\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\",\"copyRight\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\"}},\"16\":{\"modified\":\"1\",\"type\":\"quote\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"brand\":[\"11\"],\"quote\":\"cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.\",\"byline\":\"test byline\"}}},\"16\":\"\"}}','2017-01-03 04:21:50','2017-01-03 04:21:50','6e019a4d-fa77-41ed-b5f4-c54b3c036ba2'),
	(18,2,1,1,'en_us',17,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"29\":{\"15\":{\"modified\":\"0\",\"type\":\"twoColumn\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"copyLeft\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\",\"copyRight\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\"}},\"16\":{\"modified\":\"0\",\"type\":\"quote\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"brand\":[\"11\"],\"quote\":\"cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.\",\"byline\":\"test byline\"}},\"new0\":{\"modified\":\"1\",\"type\":\"imageGrid\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\"},\"new1\":{\"modified\":\"1\",\"type\":\"fullWidthImage\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"fullWidthImage\":[\"12\"]}},\"new2\":{\"modified\":\"1\",\"type\":\"fullWidthImage\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"fullWidthImage\":[\"11\"]}},\"new3\":{\"modified\":\"1\",\"type\":\"fullWidthImage\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"fullWidthImage\":[\"13\"]}}},\"16\":\"\"}}','2017-01-03 04:31:27','2017-01-03 04:31:27','70677a40-ed6d-4912-a384-7d31c7a118ef'),
	(19,2,1,1,'en_us',18,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"29\":{\"15\":{\"modified\":\"0\",\"type\":\"twoColumn\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"copyLeft\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\",\"copyRight\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\"}},\"16\":{\"modified\":\"0\",\"type\":\"quote\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"brand\":[\"11\"],\"quote\":\"cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.\",\"byline\":\"test byline\"}},\"17\":{\"modified\":\"0\",\"type\":\"imageGrid\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\"},\"18\":{\"modified\":\"0\",\"type\":\"fullWidthImage\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\"},\"new1\":{\"modified\":\"1\",\"type\":\"image\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"2\",\"fields\":{\"image\":[\"13\",\"11\",\"12\"]}},\"20\":{\"modified\":\"0\",\"type\":\"fullWidthImage\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\"},\"new0\":{\"modified\":\"1\",\"type\":\"image\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"13\"]}}},\"16\":\"\"}}','2017-01-03 04:43:20','2017-01-03 04:43:20','bffc1d89-c478-4284-a901-239acf7fdd95'),
	(20,2,1,1,'en_us',19,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"29\":{\"15\":{\"modified\":\"0\",\"type\":\"twoColumn\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"copyLeft\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\",\"copyRight\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\"}},\"16\":{\"modified\":\"0\",\"type\":\"quote\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"brand\":[\"11\"],\"quote\":\"cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.\",\"byline\":\"test byline\"}},\"20\":{\"modified\":\"0\",\"type\":\"fullWidthImage\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\"},\"22\":{\"modified\":\"0\",\"type\":\"image\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"13\"]}},\"new0\":{\"modified\":\"1\",\"type\":\"imageGrid\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\"},\"new1\":{\"modified\":\"1\",\"type\":\"image\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"13\"]}},\"new2\":{\"modified\":\"1\",\"type\":\"image\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"13\"]}},\"new3\":{\"modified\":\"1\",\"type\":\"image\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"11\"]}}},\"16\":\"\"}}','2017-01-03 04:44:55','2017-01-03 04:44:55','ff2954fc-9e87-4797-9a03-0e04dcf0b6f6'),
	(21,2,1,1,'en_us',20,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"29\":{\"15\":{\"modified\":\"0\",\"type\":\"twoColumn\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"copyLeft\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\",\"copyRight\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\"}},\"16\":{\"modified\":\"0\",\"type\":\"quote\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"brand\":[\"11\"],\"quote\":\"cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.\",\"byline\":\"test byline\"}},\"20\":{\"modified\":\"0\",\"type\":\"fullWidthImage\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\"},\"22\":{\"modified\":\"1\",\"type\":\"image\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"11\"]}},\"23\":{\"modified\":\"0\",\"type\":\"imageGrid\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\"},\"24\":{\"modified\":\"0\",\"type\":\"image\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"13\"]}},\"25\":{\"modified\":\"0\",\"type\":\"image\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"13\"]}},\"26\":{\"modified\":\"0\",\"type\":\"image\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"image\":[\"11\"]}}},\"16\":\"\"}}','2017-01-03 04:45:17','2017-01-03 04:45:17','68454baa-db96-44ea-b2bb-e1c4e998b300'),
	(22,2,1,1,'en_us',21,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"29\":{\"15\":{\"modified\":\"0\",\"type\":\"twoColumn\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"copyLeft\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\",\"copyRight\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\"}},\"16\":{\"modified\":\"0\",\"type\":\"quote\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"brand\":[\"11\"],\"quote\":\"cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.\",\"byline\":\"test byline\"}}},\"16\":\"\"}}','2017-01-03 04:46:02','2017-01-03 04:46:02','4d0c224e-38bf-407e-ba26-ee2f5e933ec3'),
	(23,2,1,1,'en_us',22,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"29\":{\"15\":{\"modified\":\"0\",\"type\":\"twoColumn\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"copyLeft\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\",\"copyRight\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\"}},\"16\":{\"modified\":\"0\",\"type\":\"quote\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"brand\":[\"11\"],\"quote\":\"cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.\",\"byline\":\"test byline\"}},\"new0\":{\"modified\":\"1\",\"type\":\"imageGrid\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"imageGrid\":[\"13\",\"11\",\"12\"]}}},\"16\":\"\"}}','2017-01-03 04:47:34','2017-01-03 04:47:34','72cd7024-cb28-40e3-b8f5-ed04b29542ea'),
	(24,2,1,1,'en_us',23,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"29\":{\"15\":{\"modified\":\"0\",\"type\":\"twoColumn\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"copyLeft\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\",\"copyRight\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\"}},\"27\":{\"modified\":\"0\",\"type\":\"imageGrid\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"imageGrid\":[\"13\",\"11\",\"12\"]}},\"16\":{\"modified\":\"0\",\"type\":\"quote\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"brand\":[\"11\"],\"quote\":\"cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.\",\"byline\":\"test byline\"}}},\"16\":\"\"}}','2017-01-03 04:49:12','2017-01-03 04:49:12','b601012e-3adb-4a78-b03a-ee48d951ad06'),
	(25,2,1,1,'en_us',24,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"29\":{\"15\":{\"modified\":\"0\",\"type\":\"twoColumn\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"copyLeft\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\",\"copyRight\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\"}},\"27\":{\"modified\":\"0\",\"type\":\"imageGrid\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"imageGrid\":[\"13\",\"11\",\"12\"]}},\"16\":{\"modified\":\"0\",\"type\":\"quote\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"brand\":[\"11\"],\"quote\":\"cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.\",\"byline\":\"test byline\"}},\"new0\":{\"modified\":\"1\",\"type\":\"cta\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"ctaLink\":[\"3\"],\"ctaText\":\"This is a link\"}}},\"16\":\"\"}}','2017-01-03 05:02:50','2017-01-03 05:02:50','3c9a3fe3-f23c-4ffe-b661-b36fb81aa143'),
	(26,2,1,1,'en_us',25,'','{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Local.craft.dev!\",\"slug\":\"homepage\",\"postDate\":1482263248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"29\":{\"15\":{\"modified\":\"0\",\"type\":\"twoColumn\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"copyLeft\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\",\"copyRight\":\"<p>Facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue! Ac eu? Auctor phasellus rhoncus mattis nascetur. Aenean etiam elit, nisi odio in. Odio! Amet mid! Odio magnis mus turpis! Lacus non, duis proin magna tincidunt? Turpis velit mus augue augue aenean arcu urna? Sociis pulvinar, habitasse! Est! Penatibus adipiscing, mauris tincidunt? Turpis odio amet sociis. Purus placerat diam! Nunc adipiscing ultricies auctor odio, turpis vel mauris lectus velit sit tincidunt turpis nunc? Dapibus facilisis a, enim cursus ridiculus lacus, cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.<\\/p>\"}},\"27\":{\"modified\":\"0\",\"type\":\"imageGrid\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"imageGrid\":[\"13\",\"11\",\"12\"]}},\"16\":{\"modified\":\"0\",\"type\":\"quote\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"brand\":[\"11\"],\"quote\":\"cursus, lorem nec nisi. Mus. Augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed, sed elementum ultricies scelerisque rhoncus ultrices massa augue. Lorem augue, egestas urna dis tristique etiam porta etiam mattis eros duis.\",\"byline\":\"test byline\"}},\"28\":{\"modified\":\"0\",\"type\":\"ctaInternal\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\",\"fields\":{\"ctaLink\":[\"3\"],\"ctaText\":\"This is a link\"}},\"new0\":{\"modified\":\"1\",\"type\":\"contentGrid\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"0\"},\"new1\":{\"modified\":\"1\",\"type\":\"contentGridItem\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"contentGridHeadline\":\"Test 1\",\"contentGridContent\":\"<p>;lkajdsf ksdf;kal;dfja sdl;kjfal;ksdjf&nbsp;<\\/p>\"}},\"new2\":{\"modified\":\"1\",\"type\":\"contentGridItem\",\"enabled\":\"1\",\"collapsed\":\"0\",\"level\":\"1\",\"fields\":{\"contentGridHeadline\":\"Test 2\",\"contentGridContent\":\"<p>;lkajdsf alkdsjfl;kajsd fkl;<\\/p>\"}}},\"16\":\"\"}}','2017-01-03 05:32:15','2017-01-03 05:32:15','45f643a7-671c-44a9-bfc0-6aa0ae0497c4');

/*!40000 ALTER TABLE `craft_entryversions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fieldgroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldgroups`;

CREATE TABLE `craft_fieldgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_fieldgroups_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldgroups` WRITE;
/*!40000 ALTER TABLE `craft_fieldgroups` DISABLE KEYS */;

INSERT INTO `craft_fieldgroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Default','2016-12-20 19:47:28','2016-12-20 19:47:28','19cd2ad5-eea7-4ff3-b1c6-09c437c8fadf'),
	(2,'Social Media','2016-12-20 19:48:33','2016-12-20 19:48:33','e2e90bfb-889d-4b04-8246-6d8f24c41f65'),
	(3,'Site Controls','2016-12-20 19:49:31','2016-12-20 19:49:31','ffd54007-3942-496b-a7ab-b3225749e7df'),
	(4,'SEO','2016-12-20 19:59:35','2016-12-20 19:59:35','b3fea85c-ddbc-488f-b0dc-fd94441ef992'),
	(5,'Google Maps','2016-12-20 20:19:40','2016-12-20 20:19:40','1563f526-a0fd-47bc-8c41-46e5c59ef3fb'),
	(6,'Content Builder','2016-12-20 21:03:19','2016-12-20 21:03:19','b2706de3-ea92-4c25-9585-5a040f9238d6'),
	(7,'Body Blocks','2017-01-03 03:59:36','2017-01-03 03:59:36','b89cca7b-657e-432b-abbc-d00aa995e098');

/*!40000 ALTER TABLE `craft_fieldgroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fieldlayoutfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldlayoutfields`;

CREATE TABLE `craft_fieldlayoutfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  KEY `craft_fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  KEY `craft_fieldlayoutfields_tabId_fk` (`tabId`),
  KEY `craft_fieldlayoutfields_fieldId_fk` (`fieldId`),
  CONSTRAINT `craft_fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `craft_fieldlayouttabs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldlayoutfields` WRITE;
/*!40000 ALTER TABLE `craft_fieldlayoutfields` DISABLE KEYS */;

INSERT INTO `craft_fieldlayoutfields` (`id`, `layoutId`, `tabId`, `fieldId`, `required`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,5,2,1,1,1,'2016-12-20 19:47:28','2016-12-20 19:47:28','44cf1a1e-5a93-46ab-85a3-1a7d73b399fd'),
	(3,5,2,2,0,2,'2016-12-20 19:47:28','2016-12-20 19:47:28','ad7e792a-da39-475c-8d6f-2eee6bc4289f'),
	(10,8,4,12,0,1,'2016-12-20 19:53:48','2016-12-20 19:53:48','2e17fe27-3409-442f-80a4-8a602124d560'),
	(11,8,4,13,0,2,'2016-12-20 19:53:48','2016-12-20 19:53:48','6d7cce23-f2b9-4393-8c5e-ee6aec0705c0'),
	(12,8,4,11,0,3,'2016-12-20 19:53:48','2016-12-20 19:53:48','785d6c56-cff3-46b6-a641-4f40617b468b'),
	(13,8,4,14,0,4,'2016-12-20 19:53:48','2016-12-20 19:53:48','ac65b906-49ed-40dd-931d-9f74670a2c7f'),
	(14,9,5,6,0,1,'2016-12-20 19:57:07','2016-12-20 19:57:07','6b3c4a67-8116-4505-882e-be51b82c764c'),
	(15,9,5,7,0,2,'2016-12-20 19:57:07','2016-12-20 19:57:07','98505130-b9a6-4349-b7da-f9de0d526eb7'),
	(16,9,5,10,0,3,'2016-12-20 19:57:07','2016-12-20 19:57:07','33b097dc-2201-4fc9-a94f-bae6a0c3c3bf'),
	(17,9,5,9,0,4,'2016-12-20 19:57:07','2016-12-20 19:57:07','cbb1ffc8-137a-43ff-a332-9ba5b002a4d0'),
	(18,9,5,15,0,5,'2016-12-20 19:57:07','2016-12-20 19:57:07','77dae403-629f-4873-b792-50311bd9118e'),
	(19,9,5,8,0,6,'2016-12-20 19:57:07','2016-12-20 19:57:07','3a82e7a9-9a00-44ef-8a24-3df7fb7af58a'),
	(20,9,5,5,0,7,'2016-12-20 19:57:07','2016-12-20 19:57:07','530864b5-fb4e-4df7-b897-8d819e8ae84b'),
	(84,49,46,21,0,1,'2016-12-20 22:27:18','2016-12-20 22:27:18','935e2998-02d4-4c6f-99da-ae99785b2b91'),
	(85,50,47,22,0,1,'2016-12-20 22:27:18','2016-12-20 22:27:18','c010bfff-6415-4f61-9c82-fd627a3fbd18'),
	(86,50,47,23,0,2,'2016-12-20 22:27:18','2016-12-20 22:27:18','7d362553-5b9f-408d-b38e-055e6f2374a2'),
	(87,51,48,24,0,1,'2016-12-20 22:27:18','2016-12-20 22:27:18','cf807692-8442-4a6a-99fc-23988e6f0d03'),
	(88,51,48,25,0,2,'2016-12-20 22:27:18','2016-12-20 22:27:18','84117a63-51eb-4b2b-b267-37e618803024'),
	(89,51,48,26,0,3,'2016-12-20 22:27:18','2016-12-20 22:27:18','9aede222-8e82-49c9-b300-fa2caa4034aa'),
	(90,52,49,27,0,1,'2016-12-20 22:27:18','2016-12-20 22:27:18','28022673-f285-4cbe-99fd-ece72b899ecd'),
	(91,53,50,28,0,1,'2016-12-20 22:27:18','2016-12-20 22:27:18','522bfe99-22e2-439b-a7a2-91cb46a07620'),
	(93,56,52,29,0,1,'2017-01-03 03:49:42','2017-01-03 03:49:42','95179224-6d36-450e-9d64-fb4b00560652'),
	(94,56,53,16,0,1,'2017-01-03 03:49:42','2017-01-03 03:49:42','7bf55e73-51a5-4a63-b484-18b26907ea0d'),
	(275,185,172,1,0,1,'2017-01-03 05:31:47','2017-01-03 05:31:47','2e057482-8a31-499b-b033-c801651b312a'),
	(276,186,173,30,0,1,'2017-01-03 05:31:47','2017-01-03 05:31:47','7ebe19df-2741-4347-be86-25edf1491205'),
	(277,186,173,31,0,2,'2017-01-03 05:31:47','2017-01-03 05:31:47','0cfd3ff9-c177-4f2d-9382-aae75817be09'),
	(278,187,174,32,0,1,'2017-01-03 05:31:47','2017-01-03 05:31:47','6b6a87b5-63c1-4b04-b6c9-c0c157e55889'),
	(279,187,174,33,0,2,'2017-01-03 05:31:47','2017-01-03 05:31:47','3e3b73e7-f255-454c-84fc-b9313737b7ab'),
	(280,187,174,34,0,3,'2017-01-03 05:31:47','2017-01-03 05:31:47','9334c2f6-eedf-45f1-8344-a557d59795c2'),
	(281,190,176,36,0,1,'2017-01-03 05:31:47','2017-01-03 05:31:47','d9b6b37c-458f-4bcf-96e7-01073a13f025'),
	(282,191,177,37,0,1,'2017-01-03 05:31:47','2017-01-03 05:31:47','36b7084a-f58e-4533-a060-bf3793cff06e'),
	(283,192,178,38,0,1,'2017-01-03 05:31:47','2017-01-03 05:31:47','4f7ea282-7b0b-4ded-98d3-2e33c8d8440e'),
	(284,192,178,39,0,2,'2017-01-03 05:31:47','2017-01-03 05:31:47','ab5be0b2-350c-4a35-8da6-5a634ec3ecaf'),
	(285,193,179,40,0,1,'2017-01-03 05:31:47','2017-01-03 05:31:47','40b9742c-3032-45e2-bfa6-53c9aba0e087'),
	(286,193,179,39,0,2,'2017-01-03 05:31:47','2017-01-03 05:31:47','5d1c5fc4-ba9f-4a00-972b-f6483c92e068'),
	(287,194,180,41,0,1,'2017-01-03 05:31:47','2017-01-03 05:31:47','7212e323-0b65-4c08-a4cf-6261dcbea5cb'),
	(288,194,180,39,0,2,'2017-01-03 05:31:47','2017-01-03 05:31:47','2a987122-b1c6-4f70-9c04-6cded78e80e6'),
	(289,195,181,42,0,1,'2017-01-03 05:31:47','2017-01-03 05:31:47','6b029215-1b08-45e6-acbc-00a4494734cf'),
	(290,195,181,43,0,2,'2017-01-03 05:31:47','2017-01-03 05:31:47','5b622cff-c65d-44d3-b6f6-3eb89eedf267');

/*!40000 ALTER TABLE `craft_fieldlayoutfields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fieldlayouts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldlayouts`;

CREATE TABLE `craft_fieldlayouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_fieldlayouts_type_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldlayouts` WRITE;
/*!40000 ALTER TABLE `craft_fieldlayouts` DISABLE KEYS */;

INSERT INTO `craft_fieldlayouts` (`id`, `type`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Tag','2016-12-20 19:47:28','2016-12-20 19:47:28','57acc433-8a96-4199-ab0a-83ec1a617f70'),
	(5,'Entry','2016-12-20 19:47:28','2016-12-20 19:47:28','f66fd482-ba4e-4ddd-af00-2032c979bd09'),
	(8,'GlobalSet','2016-12-20 19:53:48','2016-12-20 19:53:48','9191d9f4-5ad2-4a54-b5c6-350c1ee35f09'),
	(9,'GlobalSet','2016-12-20 19:57:07','2016-12-20 19:57:07','e9abf1fa-6768-4188-b98f-031a30ed2fe9'),
	(49,'MatrixBlock','2016-12-20 22:27:18','2016-12-20 22:27:18','ba3fa985-ff13-4d19-8018-1d95110aded5'),
	(50,'MatrixBlock','2016-12-20 22:27:18','2016-12-20 22:27:18','966fcde0-34f8-4fec-b265-5cf993096bd4'),
	(51,'MatrixBlock','2016-12-20 22:27:18','2016-12-20 22:27:18','cc4337fa-cafc-45d5-9ed0-e8baaddf78e4'),
	(52,'MatrixBlock','2016-12-20 22:27:18','2016-12-20 22:27:18','104c6bf7-6353-438a-a2b4-ce2926fa165d'),
	(53,'MatrixBlock','2016-12-20 22:27:18','2016-12-20 22:27:18','7addc204-c6ff-4f5b-a82a-979523bbd912'),
	(54,'Asset','2016-12-20 23:18:30','2016-12-20 23:18:30','c4eb1f9a-8c65-45cd-9997-8af54f88b4ad'),
	(56,'Entry','2017-01-03 03:49:42','2017-01-03 03:49:42','b649aacb-610c-4cdb-b8be-7dded037d295'),
	(129,'Entry','2017-01-03 05:14:29','2017-01-03 05:14:29','61ffcdfc-d107-4975-bcec-d5ce2eb6feb0'),
	(185,'Neo_Block','2017-01-03 05:31:47','2017-01-03 05:31:47','00d1819c-524d-40f7-9e8e-a6b515264b49'),
	(186,'Neo_Block','2017-01-03 05:31:47','2017-01-03 05:31:47','77c3ad34-489d-4f19-a38f-c37e9e1f1ff6'),
	(187,'Neo_Block','2017-01-03 05:31:47','2017-01-03 05:31:47','0ca4a8ff-ab5d-4f32-a580-dcaea167804d'),
	(188,'Neo_Block','2017-01-03 05:31:47','2017-01-03 05:31:47','a483cfbd-2ff6-4043-a699-c8d9bc03aef1'),
	(189,'Neo_Block','2017-01-03 05:31:47','2017-01-03 05:31:47','0e8983db-352c-4f62-b43f-bf4d39f09367'),
	(190,'Neo_Block','2017-01-03 05:31:47','2017-01-03 05:31:47','5ec414a7-7a7f-44f8-8345-8da0248e72fc'),
	(191,'Neo_Block','2017-01-03 05:31:47','2017-01-03 05:31:47','09dc4e35-8d25-48e9-99df-ec22657b8237'),
	(192,'Neo_Block','2017-01-03 05:31:47','2017-01-03 05:31:47','2d58052d-3967-46f4-b897-1b257c7aa526'),
	(193,'Neo_Block','2017-01-03 05:31:47','2017-01-03 05:31:47','92cb8d48-c48d-46f8-8983-27287d617b89'),
	(194,'Neo_Block','2017-01-03 05:31:47','2017-01-03 05:31:47','eae28692-6f40-4133-bf52-ff82afa007b9'),
	(195,'Neo_Block','2017-01-03 05:31:47','2017-01-03 05:31:47','40076950-7c31-479a-bfe6-f4919c38b4d2');

/*!40000 ALTER TABLE `craft_fieldlayouts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fieldlayouttabs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldlayouttabs`;

CREATE TABLE `craft_fieldlayouttabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  KEY `craft_fieldlayouttabs_layoutId_fk` (`layoutId`),
  CONSTRAINT `craft_fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fieldlayouttabs` WRITE;
/*!40000 ALTER TABLE `craft_fieldlayouttabs` DISABLE KEYS */;

INSERT INTO `craft_fieldlayouttabs` (`id`, `layoutId`, `name`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,5,'Content',1,'2016-12-20 19:47:28','2016-12-20 19:47:28','19d9f21f-8196-4ba5-b326-239784c850d1'),
	(4,8,'Content',1,'2016-12-20 19:53:48','2016-12-20 19:53:48','75db605d-01d9-4a7e-894f-c597e4551f11'),
	(5,9,'Content',1,'2016-12-20 19:57:07','2016-12-20 19:57:07','a578fec4-76d0-45d0-aecb-a1d8e6b402d3'),
	(46,49,'Content',1,'2016-12-20 22:27:18','2016-12-20 22:27:18','3e4861ae-5861-4d97-aee1-0ffb27253ad9'),
	(47,50,'Content',1,'2016-12-20 22:27:18','2016-12-20 22:27:18','b2a3f90e-0978-4774-9e4e-86f8eaf9e999'),
	(48,51,'Content',1,'2016-12-20 22:27:18','2016-12-20 22:27:18','b2ba47c8-9043-4d9c-b893-8f996ed9e078'),
	(49,52,'Content',1,'2016-12-20 22:27:18','2016-12-20 22:27:18','ad8004ec-4f30-4f78-8317-a25624f2200a'),
	(50,53,'Content',1,'2016-12-20 22:27:18','2016-12-20 22:27:18','b4e8b4e2-6979-4910-84c3-43e13c53d901'),
	(52,56,'Content Builder',1,'2017-01-03 03:49:42','2017-01-03 03:49:42','60935d0b-c061-4136-b87d-8509433594bf'),
	(53,56,'SEO',2,'2017-01-03 03:49:42','2017-01-03 03:49:42','3273dff2-167b-4f79-abf3-2348c33d0a86'),
	(172,185,'Tab 1',1,'2017-01-03 05:31:47','2017-01-03 05:31:47','29fcc192-9db7-4b94-8bb2-8b66ffcb43a6'),
	(173,186,'Tab 1',1,'2017-01-03 05:31:47','2017-01-03 05:31:47','dcc3b9b9-244d-41ef-894e-3bbe1759e128'),
	(174,187,'Tab 1',1,'2017-01-03 05:31:47','2017-01-03 05:31:47','2ed5de91-41ab-4e83-a507-4e16021a1658'),
	(175,189,'Tab 1',1,'2017-01-03 05:31:47','2017-01-03 05:31:47','0854aede-12de-41d9-97a4-ab5a6ddf8476'),
	(176,190,'Tab 1',1,'2017-01-03 05:31:47','2017-01-03 05:31:47','b90086fc-45c7-488d-8c5c-48b1cc27d6df'),
	(177,191,'Tab 1',1,'2017-01-03 05:31:47','2017-01-03 05:31:47','fb459eb2-b8c7-445d-9a0d-5a9e939a3b1e'),
	(178,192,'Tab 1',1,'2017-01-03 05:31:47','2017-01-03 05:31:47','1da1a388-7f64-4116-bfa8-72ec4631ed97'),
	(179,193,'Tab 1',1,'2017-01-03 05:31:47','2017-01-03 05:31:47','b14f54d3-9582-497b-a07e-8049c5eeea7d'),
	(180,194,'Tab 1',1,'2017-01-03 05:31:47','2017-01-03 05:31:47','a84b75a8-8540-4913-bbfc-622c0c3afff2'),
	(181,195,'Tab 1',1,'2017-01-03 05:31:47','2017-01-03 05:31:47','51fa6875-dc62-4a26-a001-e96fba6e5ea2');

/*!40000 ALTER TABLE `craft_fieldlayouttabs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fields`;

CREATE TABLE `craft_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(58) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'global',
  `instructions` text COLLATE utf8_unicode_ci,
  `translatable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_fields_handle_context_unq_idx` (`handle`,`context`),
  KEY `craft_fields_context_idx` (`context`),
  KEY `craft_fields_groupId_fk` (`groupId`),
  CONSTRAINT `craft_fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_fieldgroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_fields` WRITE;
/*!40000 ALTER TABLE `craft_fields` DISABLE KEYS */;

INSERT INTO `craft_fields` (`id`, `groupId`, `name`, `handle`, `context`, `instructions`, `translatable`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'Body','body','global',NULL,1,'RichText','{\"configFile\":\"Standard.json\",\"columnType\":\"text\"}','2016-12-20 19:47:28','2016-12-20 19:47:28','874f3767-79c4-4bd5-b1fb-b55988efdd10'),
	(2,1,'Tags','tags','global',NULL,0,'Tags','{\"source\":\"taggroup:1\"}','2016-12-20 19:47:28','2016-12-20 19:47:28','d431c8b8-9696-468e-810b-0ccd0e368bc9'),
	(5,2,'Flickr','flickr','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:48:55','2016-12-20 19:48:55','9cfff6e0-e6c5-48c7-805f-25c31e5ed2c7'),
	(6,2,'Facebook','facebook','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:49:05','2016-12-20 19:49:05','b2614393-1681-4418-a13a-9dd19ca04f2d'),
	(7,2,'Twitter','twitter','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:49:09','2016-12-20 19:49:09','8796aa85-8931-4f1a-bac8-faa27b435618'),
	(8,2,'Google','google','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:49:12','2016-12-20 19:49:12','5b565d7b-f17f-4c64-991c-e4e5c1cadaa9'),
	(9,2,'Pinterest','pinterest','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:49:21','2016-12-20 19:49:21','97488c60-b71a-4ff6-b60e-0c9c896b0c72'),
	(10,2,'Linkedin','linkedin','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:49:24','2016-12-20 19:49:24','543967b2-a3a4-4481-a9f5-62b6cb202d89'),
	(11,3,'Primary Telephone','primaryTelephone','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:49:58','2016-12-20 19:49:58','5ec37773-23d6-448f-8c12-e9b5f2ff51a7'),
	(12,3,'GA Code','gaCode','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:50:02','2016-12-20 19:50:02','321a9647-eca9-4a0e-9c5d-b1cbe9ff9ec0'),
	(13,3,'Primary Email','primaryEmail','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:50:07','2016-12-20 19:50:07','625fc7b4-dbfe-4277-bd7f-63ccafcc2e23'),
	(14,3,'Default SEO','defaultSeo','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:50:31','2016-12-20 19:50:31','b5360e48-fb41-44c2-aa89-96fcd7d695f2'),
	(15,2,'Instagram','instagram','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:56:57','2016-12-20 19:56:57','34811a59-a9c1-4720-8d02-cfa57401d05f'),
	(16,4,'Page Description','pageDescription','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 19:59:44','2016-12-20 19:59:44','c28e4b2f-8883-431a-9402-52d3fbc54587'),
	(18,5,'Street','street','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 20:19:57','2016-12-20 20:20:07','1bcf732c-68ce-4131-9f9d-dc67431af078'),
	(19,5,'City','city','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 20:20:01','2016-12-20 20:20:01','b4028fd2-1cae-4a84-9b4a-b44ec43cb71a'),
	(20,6,'Content Builder','contentBuilder','global','',0,'Matrix','{\"maxBlocks\":null}','2016-12-20 21:05:47','2016-12-20 22:27:18','0b737813-8bab-4bde-8617-c99f82e0594d'),
	(21,NULL,'Content','copy','matrixBlockType:1','',0,'RichText','{\"configFile\":\"Standard.json\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\"}','2016-12-20 21:05:47','2016-12-20 22:27:18','69733c1d-aea4-4967-94ca-d987ddfac8ef'),
	(22,NULL,'Content Left','copyLeft','matrixBlockType:2','',0,'RichText','{\"configFile\":\"Standard.json\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\"}','2016-12-20 21:05:47','2016-12-20 22:27:18','d4f2c039-51f7-4d9a-a2f3-723726c47033'),
	(23,NULL,'Content Right','copyRight','matrixBlockType:2','',0,'RichText','{\"configFile\":\"Standard.json\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\"}','2016-12-20 21:05:47','2016-12-20 22:27:18','077ed6f6-5727-4c2f-86e9-7a3c7f91c81d'),
	(24,NULL,'Content Left','copyLeft','matrixBlockType:3','',0,'RichText','{\"configFile\":\"Standard.json\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\"}','2016-12-20 21:51:19','2016-12-20 22:27:18','bb118001-873e-42d2-97bc-e73184c6c79f'),
	(25,NULL,'Content Middle','copyMiddle','matrixBlockType:3','',0,'RichText','{\"configFile\":\"Standard.json\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\"}','2016-12-20 21:51:19','2016-12-20 22:27:18','18832545-9e7d-4679-864a-701a5aca3399'),
	(26,NULL,'Content Right','copyRight','matrixBlockType:3','',0,'RichText','{\"configFile\":\"Standard.json\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\"}','2016-12-20 21:51:19','2016-12-20 22:27:18','3f1407a8-e309-49bf-9139-b6b4a580ea7a'),
	(27,NULL,'Code','code','matrixBlockType:4','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2016-12-20 21:59:09','2016-12-20 22:27:18','78be17a6-ecc9-44f6-900d-f72c7f4b8bb6'),
	(28,NULL,'Photos','photos','matrixBlockType:5','',0,'Assets','{\"useSingleFolder\":\"\",\"sources\":[\"folder:1\"],\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}','2016-12-20 22:16:22','2016-12-20 22:27:18','2b40bcb8-e9ca-4317-9158-8587b5f915da'),
	(29,6,'Neo Content Builder','neoContentBuilder','global','',0,'Neo','{\"maxBlocks\":null}','2017-01-03 03:48:59','2017-01-03 05:31:47','2d5d4a16-6eea-47c9-8f3b-490a4599a1f9'),
	(30,7,'Copy Left','copyLeft','global','',0,'RichText','{\"configFile\":\"\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\"}','2017-01-03 03:59:48','2017-01-03 04:02:04','325bce1d-f58f-4e9d-9eeb-72463119bf49'),
	(31,7,'Copy Right','copyRight','global','',0,'RichText','{\"configFile\":\"\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\"}','2017-01-03 03:59:58','2017-01-03 04:02:13','73af0378-cd0e-4bf2-ad0e-570aac88c0ef'),
	(32,7,'Brand','brand','global','',0,'Assets','{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}','2017-01-03 04:18:23','2017-01-03 04:18:23','c7740457-7505-49d5-ae4d-b4ecbbdd2e62'),
	(33,7,'Quote','quote','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2017-01-03 04:18:36','2017-01-03 04:18:36','a98c8d63-d188-4bad-a2e2-80fb8d467594'),
	(34,7,'Byline','byline','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2017-01-03 04:18:47','2017-01-03 04:18:47','c1e30f28-2d3e-4718-a279-6b366ae233ac'),
	(35,7,'Full Width Image','fullWidthImage','global','',0,'Assets','{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}','2017-01-03 04:29:01','2017-01-03 04:29:01','09afbcbe-b83b-49be-94e0-342d83a11a3a'),
	(36,7,'Image','image','global','',0,'Assets','{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}','2017-01-03 04:40:34','2017-01-03 04:44:07','d567819e-fb8f-4b26-90e6-58b92338e3ca'),
	(37,7,'Image Grid','imageGrid','global','',0,'Assets','{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}','2017-01-03 04:46:34','2017-01-03 04:46:34','84e73274-2cd1-40c1-8a2c-0a1f72cf3e8b'),
	(38,7,'CTA Link','ctaLink','global','',0,'Entries','{\"sources\":\"*\",\"limit\":\"1\",\"selectionLabel\":\"\"}','2017-01-03 05:01:41','2017-01-03 05:03:55','b15ba9e4-d5c5-4826-a685-6a6ff19a332a'),
	(39,7,'CTA Text','ctaText','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2017-01-03 05:01:49','2017-01-03 05:01:49','7eb20126-f875-4ddb-a482-c0f0b9716b3d'),
	(40,7,'CTA Link External','ctaLinkExternal','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2017-01-03 05:11:51','2017-01-03 05:11:51','6f9c101f-13cc-4703-9d2b-9ef7efe75787'),
	(41,7,'CTA Link Landing Page','ctaLinkLanding','global','',0,'Entries','{\"sources\":[\"section:3\"],\"limit\":\"1\",\"selectionLabel\":\"\"}','2017-01-03 05:12:58','2017-01-03 05:26:53','b2bc7b84-df1d-4d3f-ba9f-bf5103b22a1a'),
	(42,7,'Headline','contentGridHeadline','global','',0,'PlainText','{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}','2017-01-03 05:27:15','2017-01-03 05:30:07','91a9b8fe-cdd2-4e13-a8d0-4e32afb8780b'),
	(43,7,'Content','contentGridContent','global','',0,'RichText','{\"configFile\":\"\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\"}','2017-01-03 05:27:41','2017-01-03 05:29:58','c43e2b7b-eb25-4c8d-9316-20b780a6f56b');

/*!40000 ALTER TABLE `craft_fields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_globalsets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_globalsets`;

CREATE TABLE `craft_globalsets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_globalsets_name_unq_idx` (`name`),
  UNIQUE KEY `craft_globalsets_handle_unq_idx` (`handle`),
  KEY `craft_globalsets_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_globalsets` WRITE;
/*!40000 ALTER TABLE `craft_globalsets` DISABLE KEYS */;

INSERT INTO `craft_globalsets` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(4,'Social Media','socialMedia',9,'2016-12-20 19:48:12','2016-12-20 19:57:07','5e7c24d4-f38b-4978-83c2-b4b58ab73cf3'),
	(5,'Site Controls','siteControls',8,'2016-12-20 19:53:48','2016-12-20 19:53:48','eb6d7773-0171-48bb-982c-196bd9d992cf');

/*!40000 ALTER TABLE `craft_globalsets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_info`;

CREATE TABLE `craft_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `edition` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `siteName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `siteUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `on` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `maintenance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_info` WRITE;
/*!40000 ALTER TABLE `craft_info` DISABLE KEYS */;

INSERT INTO `craft_info` (`id`, `version`, `schemaVersion`, `edition`, `siteName`, `siteUrl`, `timezone`, `on`, `maintenance`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'2.6.2954','2.6.9',0,'Local Craft','http://local.craft.dev','UTC',1,0,'2016-12-20 19:47:24','2016-12-20 19:47:24','b8a3bc43-e7ba-42c4-9b93-41c2b0978a5e');

/*!40000 ALTER TABLE `craft_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_leads
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_leads`;

CREATE TABLE `craft_leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_locales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_locales`;

CREATE TABLE `craft_locales` (
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`locale`),
  KEY `craft_locales_sortOrder_idx` (`sortOrder`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_locales` WRITE;
/*!40000 ALTER TABLE `craft_locales` DISABLE KEYS */;

INSERT INTO `craft_locales` (`locale`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	('en_us',1,'2016-12-20 19:47:24','2016-12-20 19:47:24','3ded7672-9584-498e-9558-c566ad6dc8dc');

/*!40000 ALTER TABLE `craft_locales` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_matrixblocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_matrixblocks`;

CREATE TABLE `craft_matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `ownerLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_matrixblocks_ownerId_idx` (`ownerId`),
  KEY `craft_matrixblocks_fieldId_idx` (`fieldId`),
  KEY `craft_matrixblocks_typeId_idx` (`typeId`),
  KEY `craft_matrixblocks_sortOrder_idx` (`sortOrder`),
  KEY `craft_matrixblocks_ownerLocale_fk` (`ownerLocale`),
  CONSTRAINT `craft_matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocks_ownerLocale_fk` FOREIGN KEY (`ownerLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `craft_matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_matrixblocks` WRITE;
/*!40000 ALTER TABLE `craft_matrixblocks` DISABLE KEYS */;

INSERT INTO `craft_matrixblocks` (`id`, `ownerId`, `fieldId`, `typeId`, `sortOrder`, `ownerLocale`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(6,2,20,1,2,NULL,'2016-12-20 21:17:19','2016-12-20 22:21:48','a4eab007-203a-43d2-ae39-1a335f982469'),
	(7,2,20,2,1,NULL,'2016-12-20 21:17:19','2016-12-20 22:21:48','70fc193c-1a0e-441d-b2d5-e04fc7c691be'),
	(8,2,20,3,3,NULL,'2016-12-20 21:52:55','2016-12-20 22:21:48','19c92bab-5fae-492b-aaa9-3cb61389c5f8'),
	(9,2,20,3,4,NULL,'2016-12-20 21:53:39','2016-12-20 22:21:48','b8075e45-08a3-4ced-a1bd-6cf26f8f7930'),
	(10,2,20,5,5,NULL,'2016-12-20 22:18:51','2016-12-20 22:21:48','d12a0cf0-dc0c-4827-b0e8-60debca991bc');

/*!40000 ALTER TABLE `craft_matrixblocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_matrixblocktypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_matrixblocktypes`;

CREATE TABLE `craft_matrixblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  UNIQUE KEY `craft_matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `craft_matrixblocktypes_fieldId_fk` (`fieldId`),
  KEY `craft_matrixblocktypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_matrixblocktypes` WRITE;
/*!40000 ALTER TABLE `craft_matrixblocktypes` DISABLE KEYS */;

INSERT INTO `craft_matrixblocktypes` (`id`, `fieldId`, `fieldLayoutId`, `name`, `handle`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,20,49,'Full Width','fullCopy',1,'2016-12-20 21:05:47','2016-12-20 22:27:18','7f567c6b-a0b3-4531-9369-ba539971fb50'),
	(2,20,50,'Two Column','twoColumnCopy',2,'2016-12-20 21:05:47','2016-12-20 22:27:18','ac12f441-ff2b-4ada-b8df-9f4e92ac2402'),
	(3,20,51,'Three Column','threeColumnCopy',3,'2016-12-20 21:51:19','2016-12-20 22:27:18','e4dd6d80-b5f0-4092-9dc9-02474f96a2d6'),
	(4,20,52,'Code Snippet','codeSnippet',4,'2016-12-20 21:59:09','2016-12-20 22:27:18','48cc282d-b604-4ac1-b84e-5b5757dcfc2a'),
	(5,20,53,'Photo Gallery','photoGallery',5,'2016-12-20 22:16:22','2016-12-20 22:27:18','f70425fc-ee45-47f2-be6a-65fb21555c4b');

/*!40000 ALTER TABLE `craft_matrixblocktypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_matrixcontent_contentbuilder
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_matrixcontent_contentbuilder`;

CREATE TABLE `craft_matrixcontent_contentbuilder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_fullCopy_copy` text COLLATE utf8_unicode_ci,
  `field_twoColumnCopy_copyLeft` text COLLATE utf8_unicode_ci,
  `field_twoColumnCopy_copyRight` text COLLATE utf8_unicode_ci,
  `field_threeColumnCopy_copyLeft` text COLLATE utf8_unicode_ci,
  `field_threeColumnCopy_copyMiddle` text COLLATE utf8_unicode_ci,
  `field_threeColumnCopy_copyRight` text COLLATE utf8_unicode_ci,
  `field_codeSnippet_code` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_matrixcontent_contentbuilder_elementId_locale_unq_idx` (`elementId`,`locale`),
  KEY `craft_matrixcontent_contentbuilder_locale_fk` (`locale`),
  CONSTRAINT `craft_matrixcontent_contentbuilder_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixcontent_contentbuilder_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_matrixcontent_contentbuilder` WRITE;
/*!40000 ALTER TABLE `craft_matrixcontent_contentbuilder` DISABLE KEYS */;

INSERT INTO `craft_matrixcontent_contentbuilder` (`id`, `elementId`, `locale`, `field_fullCopy_copy`, `field_twoColumnCopy_copyLeft`, `field_twoColumnCopy_copyRight`, `field_threeColumnCopy_copyLeft`, `field_threeColumnCopy_copyMiddle`, `field_threeColumnCopy_copyRight`, `field_codeSnippet_code`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,6,'en_us','<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>',NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 21:17:19','2016-12-20 22:21:48','d92f9bc7-b564-488d-afce-a8c7ffd8274b'),
	(2,7,'en_us',NULL,'<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>','<p>Ultricies sociis pulvinar montes adipiscing nascetur porttitor ut! Elementum magna, elit, duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies, ultrices lectus sit! Turpis vel et! Placerat integer penatibus tincidunt, elit mus enim ac urna cursus porttitor lectus? Elementum magna odio! Aliquam. Odio aliquam, montes et integer ut dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>',NULL,NULL,NULL,NULL,'2016-12-20 21:17:19','2016-12-20 22:21:48','eec51011-d4f5-4f5d-944b-aaa3b4e73978'),
	(3,8,'en_us',NULL,NULL,NULL,'<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>','<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>','<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>',NULL,'2016-12-20 21:52:55','2016-12-20 22:21:48','0167a326-a8cb-4eeb-b5fb-44ac1157ab61'),
	(4,9,'en_us',NULL,NULL,NULL,'<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>','<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>','<p>dis placerat dolor a sagittis eu integer nec? Ultrices etiam. Sit, purus ut diam sed, et sed odio! Elementum pid? Pulvinar eu proin non et etiam. A pid! Augue mauris in vel, in, egestas habitasse natoque auctor porta? Elit nascetur amet magnis urna enim, purus ridiculus massa sed et natoque.</p>',NULL,'2016-12-20 21:53:39','2016-12-20 22:21:48','29854e39-2a3c-4a35-97de-f25da6222938'),
	(5,10,'en_us',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2016-12-20 22:18:51','2016-12-20 22:21:48','21bea68b-36ed-40b1-97fa-f3abfe645dec');

/*!40000 ALTER TABLE `craft_matrixcontent_contentbuilder` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_migrations`;

CREATE TABLE `craft_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pluginId` int(11) DEFAULT NULL,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_migrations_version_unq_idx` (`version`),
  KEY `craft_migrations_pluginId_fk` (`pluginId`),
  CONSTRAINT `craft_migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `craft_plugins` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_migrations` WRITE;
/*!40000 ALTER TABLE `craft_migrations` DISABLE KEYS */;

INSERT INTO `craft_migrations` (`id`, `pluginId`, `version`, `applyTime`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,'m000000_000000_base','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','81136106-f88f-490d-bbbd-2abfe6025bf6'),
	(2,NULL,'m140730_000001_add_filename_and_format_to_transformindex','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','998b0367-9458-486c-a2ef-9e3c46fb2b94'),
	(3,NULL,'m140815_000001_add_format_to_transforms','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','5ecdc0ac-157a-43f9-a24d-cbbfb476049b'),
	(4,NULL,'m140822_000001_allow_more_than_128_items_per_field','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','5ec71ebd-2e18-45b7-bb7b-f9d64a55f121'),
	(5,NULL,'m140829_000001_single_title_formats','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','75e72af3-737b-478a-bb07-730c94791662'),
	(6,NULL,'m140831_000001_extended_cache_keys','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','b75eb466-52f2-4c5d-80a1-60025775f233'),
	(7,NULL,'m140922_000001_delete_orphaned_matrix_blocks','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','e7638bcc-7cf5-4d51-abff-55121ca23a80'),
	(8,NULL,'m141008_000001_elements_index_tune','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','3e2ca63e-ab5d-443b-a3d3-5343abb522a2'),
	(9,NULL,'m141009_000001_assets_source_handle','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','50343f76-d626-466a-8b0e-90565edbe021'),
	(10,NULL,'m141024_000001_field_layout_tabs','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','50b2b7a3-f005-45b9-86cf-5799c1fcb697'),
	(11,NULL,'m141030_000000_plugin_schema_versions','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','120755f5-a29d-45d1-822f-38837f4b6ee9'),
	(12,NULL,'m141030_000001_drop_structure_move_permission','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','b5804895-4a40-44dc-8a58-04bc3adcb8aa'),
	(13,NULL,'m141103_000001_tag_titles','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','447a56e3-d4bc-4f66-a359-70e86d90651b'),
	(14,NULL,'m141109_000001_user_status_shuffle','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','47ada619-aff7-461f-8313-062d54086094'),
	(15,NULL,'m141126_000001_user_week_start_day','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','b5cec8db-f2cc-4fe0-93bb-30917165dd7a'),
	(16,NULL,'m150210_000001_adjust_user_photo_size','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','f0837d36-4763-4dc2-b941-4abe1de103e1'),
	(17,NULL,'m150724_000001_adjust_quality_settings','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','72a9bc4a-c2d7-42c3-93d9-53dd4d1e2abc'),
	(18,NULL,'m150827_000000_element_index_settings','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','2476b917-9576-4b31-8292-d8ebc516a573'),
	(19,NULL,'m150918_000001_add_colspan_to_widgets','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','c26d4634-1055-494e-82ac-57a2515e358e'),
	(20,NULL,'m151007_000000_clear_asset_caches','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','41b8b36a-4131-4fc1-96ca-ea33e6ca81ed'),
	(21,NULL,'m151109_000000_text_url_formats','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','fb866ffd-5ac6-4737-8212-edc0102286ab'),
	(22,NULL,'m151110_000000_move_logo','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','1d9ec314-bf46-4646-88f2-1ef0da48beca'),
	(23,NULL,'m151117_000000_adjust_image_widthheight','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','4e5b2be3-a938-4863-b733-220b686b778e'),
	(24,NULL,'m151127_000000_clear_license_key_status','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','ba6b62d5-8ca8-4a0e-a2b9-428d9c54dc4a'),
	(25,NULL,'m151127_000000_plugin_license_keys','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','69c66fce-d189-459b-8ba2-766f6c7da672'),
	(26,NULL,'m151130_000000_update_pt_widget_feeds','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','203ca132-f8f9-4de5-8414-8da166e261ec'),
	(27,NULL,'m160114_000000_asset_sources_public_url_default_true','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','678b9680-d3f7-446f-94f6-35f17c699456'),
	(28,NULL,'m160223_000000_sortorder_to_smallint','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','aa4fbeb7-dfe9-4c20-8d6d-13a4fbf4b4ae'),
	(29,NULL,'m160229_000000_set_default_entry_statuses','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','826453de-efaf-45df-9d59-68cd8516fe9e'),
	(30,NULL,'m160304_000000_client_permissions','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','db385a8a-e2c6-4f87-84d1-981496e5905c'),
	(31,NULL,'m160322_000000_asset_filesize','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','5c8940ac-e13e-4639-a545-6b692e6f75d6'),
	(32,NULL,'m160503_000000_orphaned_fieldlayouts','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','fe4bc8be-ce78-4a54-bc8b-d62eb391810d'),
	(33,NULL,'m160510_000000_tasksettings','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','e0e008ed-bb56-46c1-90c1-8224f051a47f'),
	(34,NULL,'m160829_000000_pending_user_content_cleanup','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','b881082d-c3ed-4cc0-a239-5b5afaf393e7'),
	(35,NULL,'m160830_000000_asset_index_uri_increase','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','cc84c475-bd75-4e76-afb8-8bdc4d85907c'),
	(36,NULL,'m160919_000000_usergroup_handle_title_unique','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','d5197075-cc4d-47ce-90d5-634e5261af0d'),
	(37,NULL,'m161108_000000_new_version_format','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','5bc97a3b-78fc-4466-aa21-e3b892571d2c'),
	(38,NULL,'m161109_000000_index_shuffle','2016-12-20 19:47:24','2016-12-20 19:47:24','2016-12-20 19:47:24','7f3e93e9-9e2d-466d-bda3-2ec1c898366a'),
	(39,3,'m151016_151424_pimpmymatrix_add_block_types_table','2016-12-20 21:39:17','2016-12-20 21:39:17','2016-12-20 21:39:17','21ea5fbc-8b26-4010-91f8-0775460f62c9'),
	(40,3,'m151027_103010_pimpmymatrix_migrate_old_settings','2016-12-20 21:39:17','2016-12-20 21:39:17','2016-12-20 21:39:17','2b871a77-7870-4286-b821-9b0fcf89a825'),
	(41,4,'m160428_202308_Neo_UpdateBlockLevels','2017-01-03 02:29:18','2017-01-03 02:29:18','2017-01-03 02:29:18','5119711b-18d8-47e2-9667-36bb18a5fcc3'),
	(42,4,'m160515_005002_Neo_UpdateBlockStructure','2017-01-03 02:29:18','2017-01-03 02:29:18','2017-01-03 02:29:18','51c5fd75-e062-4afb-92e8-5c7ef22e72a2'),
	(43,4,'m160605_191540_Neo_UpdateBlockStructureLocales','2017-01-03 02:29:18','2017-01-03 02:29:18','2017-01-03 02:29:18','4b288b7b-87ea-4701-836e-fee84351a5e6'),
	(44,4,'m161029_230849_Neo_AddMaxChildBlocksSetting','2017-01-03 02:29:18','2017-01-03 02:29:18','2017-01-03 02:29:18','e87cf9b1-db06-4630-9ee7-8078f696c914');

/*!40000 ALTER TABLE `craft_migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_neoblocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_neoblocks`;

CREATE TABLE `craft_neoblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `collapsed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ownerLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_neoblocks_ownerId_idx` (`ownerId`),
  KEY `craft_neoblocks_fieldId_idx` (`fieldId`),
  KEY `craft_neoblocks_typeId_idx` (`typeId`),
  KEY `craft_neoblocks_collapsed_idx` (`collapsed`),
  KEY `craft_neoblocks_ownerLocale_fk` (`ownerLocale`),
  CONSTRAINT `craft_neoblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `craft_neoblocktypes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_neoblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_neoblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_neoblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_neoblocks_ownerLocale_fk` FOREIGN KEY (`ownerLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_neoblocks` WRITE;
/*!40000 ALTER TABLE `craft_neoblocks` DISABLE KEYS */;

INSERT INTO `craft_neoblocks` (`id`, `ownerId`, `fieldId`, `typeId`, `collapsed`, `ownerLocale`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(15,2,29,2,0,NULL,'2017-01-03 04:01:34','2017-01-03 04:01:34','38dc6c9a-7824-41a5-bbd3-0de80b4ff362'),
	(16,2,29,3,0,NULL,'2017-01-03 04:19:50','2017-01-03 04:21:50','abab8c49-52f0-4b95-b75d-82424eb8d840'),
	(27,2,29,5,0,NULL,'2017-01-03 04:47:34','2017-01-03 04:47:34','64e48255-4a3d-49c6-a353-9c7b21bbf7ff'),
	(28,2,29,7,0,NULL,'2017-01-03 05:02:50','2017-01-03 05:02:50','11f61490-e102-458e-b3ed-2f9d4c7b69da'),
	(29,2,29,10,0,NULL,'2017-01-03 05:32:15','2017-01-03 05:32:15','a66dcaa5-e6aa-4006-bd6f-06012771973f'),
	(30,2,29,11,0,NULL,'2017-01-03 05:32:15','2017-01-03 05:32:15','82a6bea7-5421-4adb-83d6-2a3438eec8a3'),
	(31,2,29,11,0,NULL,'2017-01-03 05:32:15','2017-01-03 05:32:15','0a9fd8fe-c179-433d-9e5f-5386751d9afd');

/*!40000 ALTER TABLE `craft_neoblocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_neoblockstructures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_neoblockstructures`;

CREATE TABLE `craft_neoblockstructures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `ownerLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_neoblockstructures_structureId_idx` (`structureId`),
  KEY `craft_neoblockstructures_ownerId_idx` (`ownerId`),
  KEY `craft_neoblockstructures_fieldId_idx` (`fieldId`),
  KEY `craft_neoblockstructures_ownerLocale_fk` (`ownerLocale`),
  CONSTRAINT `craft_neoblockstructures_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_neoblockstructures_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_neoblockstructures_ownerLocale_fk` FOREIGN KEY (`ownerLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_neoblockstructures_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_neoblockstructures` WRITE;
/*!40000 ALTER TABLE `craft_neoblockstructures` DISABLE KEYS */;

INSERT INTO `craft_neoblockstructures` (`id`, `structureId`, `ownerId`, `fieldId`, `ownerLocale`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(14,14,2,29,NULL,'2017-01-03 05:32:15','2017-01-03 05:32:15','b199376a-12e8-4532-b62b-de21fe33b213');

/*!40000 ALTER TABLE `craft_neoblockstructures` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_neoblocktypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_neoblocktypes`;

CREATE TABLE `craft_neoblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `maxBlocks` int(10) DEFAULT '0',
  `maxChildBlocks` int(10) DEFAULT '0',
  `childBlocks` text COLLATE utf8_unicode_ci,
  `topLevel` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_neoblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  UNIQUE KEY `craft_neoblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `craft_neoblocktypes_fieldId_fk` (`fieldId`),
  KEY `craft_neoblocktypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_neoblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_neoblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_neoblocktypes` WRITE;
/*!40000 ALTER TABLE `craft_neoblocktypes` DISABLE KEYS */;

INSERT INTO `craft_neoblocktypes` (`id`, `fieldId`, `fieldLayoutId`, `name`, `handle`, `maxBlocks`, `maxChildBlocks`, `childBlocks`, `topLevel`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,29,185,'Text Field','textField',NULL,NULL,'[\"textField\"]',1,2,'2017-01-03 03:48:59','2017-01-03 05:31:47','f3a6da36-0a6b-4b08-82c4-c39ec487a8ca'),
	(2,29,186,'Two Column','twoColumn',NULL,NULL,'',1,3,'2017-01-03 03:59:11','2017-01-03 05:31:47','5e8abd1d-312d-4410-b40a-aa966069b51d'),
	(3,29,187,'Quote','quote',NULL,NULL,'',1,4,'2017-01-03 04:19:28','2017-01-03 05:31:47','ad340643-af95-4f45-8f6c-d855116e24f7'),
	(4,29,189,'Full WIdth Image','fullWidthImage',NULL,1,'[\"image\"]',1,8,'2017-01-03 04:29:33','2017-01-03 05:31:47','6298eed8-587c-495f-ac27-4a2354dd5c62'),
	(5,29,191,'Image Grid','imageGrid',NULL,NULL,'',1,10,'2017-01-03 04:30:29','2017-01-03 05:31:47','50ac954d-034b-4f1c-8c21-72679ce294da'),
	(6,29,190,'Image','image',NULL,NULL,'',0,9,'2017-01-03 04:41:10','2017-01-03 05:31:47','8ffcc0ca-4a43-4d9a-8b19-cd03b2158752'),
	(7,29,192,'Internal','ctaInternal',NULL,NULL,'',1,12,'2017-01-03 05:02:24','2017-01-03 05:31:47','03668a27-ea80-4e9f-a7db-8751876bb49d'),
	(8,29,193,'External','ctaExternal',NULL,NULL,'',1,13,'2017-01-03 05:17:39','2017-01-03 05:31:47','99495177-8d1d-4d6e-8f04-325164d375c0'),
	(9,29,194,'Landing Page','ctaLanding',NULL,NULL,'',1,14,'2017-01-03 05:17:39','2017-01-03 05:31:47','75a75828-bbd9-474f-aa46-cacd9651f666'),
	(10,29,188,'Content Grid','contentGrid',NULL,NULL,'[\"contentGridItem\"]',1,5,'2017-01-03 05:30:34','2017-01-03 05:31:47','5905a0df-6d4c-4e8a-ae35-011a20db587d'),
	(11,29,195,'Content Grid Item','contentGridItem',NULL,NULL,'',0,6,'2017-01-03 05:31:24','2017-01-03 05:31:47','83f1f215-60dd-4d98-bec9-50d490d87f6e');

/*!40000 ALTER TABLE `craft_neoblocktypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_neogroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_neogroups`;

CREATE TABLE `craft_neogroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_neogroups_fieldId_fk` (`fieldId`),
  CONSTRAINT `craft_neogroups_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_neogroups` WRITE;
/*!40000 ALTER TABLE `craft_neogroups` DISABLE KEYS */;

INSERT INTO `craft_neogroups` (`id`, `fieldId`, `name`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(44,29,'Content',1,'2017-01-03 05:31:47','2017-01-03 05:31:47','cff1aca3-5466-4f2f-963d-6c8ef27f8201'),
	(45,29,'Images',7,'2017-01-03 05:31:47','2017-01-03 05:31:47','5e441881-e841-4451-a4a7-622af1df4580'),
	(46,29,'CTAs',11,'2017-01-03 05:31:47','2017-01-03 05:31:47','03e0866b-8334-413a-9953-6ca85a14d2ec');

/*!40000 ALTER TABLE `craft_neogroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_pimpmymatrix_blocktypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_pimpmymatrix_blocktypes`;

CREATE TABLE `craft_pimpmymatrix_blocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `matrixBlockTypeId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `groupName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_pimpmymatrix_blocktypes_fieldId_fk` (`fieldId`),
  KEY `craft_pimpmymatrix_blocktypes_matrixBlockTypeId_fk` (`matrixBlockTypeId`),
  KEY `craft_pimpmymatrix_blocktypes_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_pimpmymatrix_blocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_pimpmymatrix_blocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_pimpmymatrix_blocktypes_matrixBlockTypeId_fk` FOREIGN KEY (`matrixBlockTypeId`) REFERENCES `craft_matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_pimpmymatrix_blocktypes` WRITE;
/*!40000 ALTER TABLE `craft_pimpmymatrix_blocktypes` DISABLE KEYS */;

INSERT INTO `craft_pimpmymatrix_blocktypes` (`id`, `fieldId`, `matrixBlockTypeId`, `fieldLayoutId`, `groupName`, `context`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(10,20,1,NULL,'Content','global','2016-12-20 22:18:18','2016-12-20 22:18:18','8be5421c-8470-4bfd-b3ac-472a52b8bb79'),
	(11,20,2,NULL,'Content','global','2016-12-20 22:18:18','2016-12-20 22:18:18','b707dfab-7001-4e91-90e0-3375ba0e1e1b'),
	(12,20,3,NULL,'Content','global','2016-12-20 22:18:18','2016-12-20 22:18:18','0e91475c-9474-489e-8c67-933588898301'),
	(13,20,4,NULL,'Content','global','2016-12-20 22:18:18','2016-12-20 22:18:18','62a24857-6faf-42ea-8941-c8bd4adb1836'),
	(14,20,5,NULL,'Images','global','2016-12-20 22:18:18','2016-12-20 22:18:18','f0a4e728-9cb1-407c-99b6-ca8b736f74ad');

/*!40000 ALTER TABLE `craft_pimpmymatrix_blocktypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_plugins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_plugins`;

CREATE TABLE `craft_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKey` char(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','unknown') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `settings` text COLLATE utf8_unicode_ci,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_plugins` WRITE;
/*!40000 ALTER TABLE `craft_plugins` DISABLE KEYS */;

INSERT INTO `craft_plugins` (`id`, `class`, `version`, `schemaVersion`, `licenseKey`, `licenseKeyStatus`, `enabled`, `settings`, `installDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Leads','1.0',NULL,NULL,'unknown',1,'{\"toEmail\":\"justin.lobaito@weloideas.com\",\"adminSubject\":\"New message from Local Craft\",\"guestSubject\":\"Hello from Local Craft\",\"welcomeEmailMessage\":\"<p>Hi {{name}} &mdash; thank you for reaching out to us!<br>We wanted to drop you a quick note that we\'ve received your information and we\'ll be in touch very soon!<\\/p><p>Regards,<br>Local Craft<\\/p>\"}','2016-12-20 20:49:09','2016-12-20 20:49:09','2017-01-03 02:29:28','8a300c1e-7c46-40e1-bae5-b5f20ce507ac'),
	(2,'Blueprint','0.5',NULL,NULL,'unknown',1,NULL,'2016-12-20 21:35:25','2016-12-20 21:35:25','2017-01-03 02:29:28','b0453b63-6478-46e5-a4d7-c8725c257738'),
	(3,'PimpMyMatrix','2.1.2','2.0.0',NULL,'unknown',1,NULL,'2016-12-20 21:39:17','2016-12-20 21:39:17','2017-01-03 02:29:28','117eea4c-eb69-4abf-8e79-8e41a6d2506e'),
	(4,'Neo','1.4.0','1.4.0',NULL,'unknown',1,NULL,'2017-01-03 02:29:18','2017-01-03 02:29:18','2017-01-03 02:29:28','3b6f4c9d-3de0-48aa-b0df-685ff27dfff5'),
	(5,'QuickField','0.3.4','1.0.0',NULL,'unknown',1,NULL,'2017-01-03 05:45:33','2017-01-03 05:45:33','2017-01-03 05:45:33','43f98355-e41b-4f44-beb0-b9ff65d682fb');

/*!40000 ALTER TABLE `craft_plugins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_rackspaceaccess
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_rackspaceaccess`;

CREATE TABLE `craft_rackspaceaccess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `connectionKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `storageUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cdnUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_rackspaceaccess_connectionKey_unq_idx` (`connectionKey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_relations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_relations`;

CREATE TABLE `craft_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_relations_fieldId_sourceId_sourceLocale_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceLocale`,`targetId`),
  KEY `craft_relations_sourceId_fk` (`sourceId`),
  KEY `craft_relations_sourceLocale_fk` (`sourceLocale`),
  KEY `craft_relations_targetId_fk` (`targetId`),
  CONSTRAINT `craft_relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_relations_sourceLocale_fk` FOREIGN KEY (`sourceLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_relations` WRITE;
/*!40000 ALTER TABLE `craft_relations` DISABLE KEYS */;

INSERT INTO `craft_relations` (`id`, `fieldId`, `sourceId`, `sourceLocale`, `targetId`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(4,28,10,NULL,13,1,'2016-12-20 22:21:48','2016-12-20 22:21:48','923df263-5bf0-4e1d-ae0c-c426b8f02a43'),
	(5,28,10,NULL,11,2,'2016-12-20 22:21:48','2016-12-20 22:21:48','dbe8e064-615a-470d-b9b6-366bb1fddffa'),
	(6,28,10,NULL,12,3,'2016-12-20 22:21:48','2016-12-20 22:21:48','07d626f1-e203-41c9-bcbd-77ea3da29039'),
	(7,32,16,NULL,11,1,'2017-01-03 04:21:50','2017-01-03 04:21:50','c0e690aa-22b0-4826-bab6-914681d23fcb'),
	(19,37,27,NULL,13,1,'2017-01-03 04:47:34','2017-01-03 04:47:34','dc189722-ece2-4005-8e5a-0e13fe7701d1'),
	(20,37,27,NULL,11,2,'2017-01-03 04:47:34','2017-01-03 04:47:34','a6f4cd86-e195-4c07-98e8-d907c0245056'),
	(21,37,27,NULL,12,3,'2017-01-03 04:47:34','2017-01-03 04:47:34','ab61f68e-eb11-4f77-bdc6-c52a4411e274'),
	(22,38,28,NULL,3,1,'2017-01-03 05:02:50','2017-01-03 05:02:50','658a3293-c15d-4519-964d-9244a2da96f5');

/*!40000 ALTER TABLE `craft_relations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_routes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_routes`;

CREATE TABLE `craft_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `urlParts` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `urlPattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_routes_urlPattern_unq_idx` (`urlPattern`),
  KEY `craft_routes_locale_idx` (`locale`),
  CONSTRAINT `craft_routes_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_searchindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_searchindex`;

CREATE TABLE `craft_searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `fieldId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`locale`),
  FULLTEXT KEY `craft_searchindex_keywords_idx` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_searchindex` WRITE;
/*!40000 ALTER TABLE `craft_searchindex` DISABLE KEYS */;

INSERT INTO `craft_searchindex` (`elementId`, `attribute`, `fieldId`, `locale`, `keywords`)
VALUES
	(1,'username',0,'en_us',' admin '),
	(1,'firstname',0,'en_us',''),
	(1,'lastname',0,'en_us',''),
	(1,'fullname',0,'en_us',''),
	(1,'email',0,'en_us',' justin lobaito weloideas com '),
	(1,'slug',0,'en_us',''),
	(2,'slug',0,'en_us',' homepage '),
	(2,'title',0,'en_us',' welcome to local craft dev '),
	(2,'field',1,'en_us',' it s true this site doesn t have a whole lot of content yet but don t worry our web developers have just installed the cms and they re setting things up for the content editors this very moment soon local craft dev will be an oasis of fresh perspectives sharp analyses and astute opinions that will keep you coming back again and again '),
	(3,'field',1,'en_us',' craft is the cms that s powering local craft dev it s beautiful powerful flexible and easy to use and it s made by pixel tonic we can t wait to dive in and see what it s capable of this is even more captivating content which you couldn t see on the news index page because it was entered after a page break and the news index template only likes to show the content on the first page craft a nice alternative to word if you re making a website '),
	(3,'field',2,'en_us',''),
	(3,'slug',0,'en_us',' we just installed craft '),
	(3,'title',0,'en_us',' we just installed craft '),
	(4,'slug',0,'en_us',''),
	(4,'field',6,'en_us',' test '),
	(4,'field',7,'en_us',''),
	(4,'field',10,'en_us',''),
	(4,'field',9,'en_us',''),
	(4,'field',8,'en_us',''),
	(4,'field',5,'en_us',''),
	(5,'field',12,'en_us',' test '),
	(5,'field',13,'en_us',''),
	(5,'field',11,'en_us',''),
	(5,'field',14,'en_us',''),
	(5,'slug',0,'en_us',''),
	(4,'field',15,'en_us',''),
	(2,'field',17,'en_us',' test '),
	(2,'field',16,'en_us',''),
	(2,'field',20,'en_us',' ultricies sociis pulvinar montes adipiscing nascetur porttitor ut elementum magna elit duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies ultrices lectus sit turpis vel et placerat integer penatibus tincidunt elit mus enim ac urna cursus porttitor lectus elementum magna odio aliquam odio aliquam montes et integer ut dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque ultricies sociis pulvinar montes adipiscing nascetur porttitor ut elementum magna elit duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies ultrices lectus sit turpis vel et placerat integer penatibus tincidunt elit mus enim ac urna cursus porttitor lectus elementum magna odio aliquam odio aliquam montes et integer ut dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque ultricies sociis pulvinar montes adipiscing nascetur porttitor ut elementum magna elit duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies ultrices lectus sit turpis vel et placerat integer penatibus tincidunt elit mus enim ac urna cursus porttitor lectus elementum magna odio aliquam odio aliquam montes et integer ut dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque screen shot 2016 12 19 at 4 40 10 pm screen shot 2016 12 20 at 12 31 37 pm screen shot 2016 12 20 at 9 42 04 am '),
	(6,'field',21,'en_us',' ultricies sociis pulvinar montes adipiscing nascetur porttitor ut elementum magna elit duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies ultrices lectus sit turpis vel et placerat integer penatibus tincidunt elit mus enim ac urna cursus porttitor lectus elementum magna odio aliquam odio aliquam montes et integer ut dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(6,'slug',0,'en_us',''),
	(7,'field',22,'en_us',' ultricies sociis pulvinar montes adipiscing nascetur porttitor ut elementum magna elit duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies ultrices lectus sit turpis vel et placerat integer penatibus tincidunt elit mus enim ac urna cursus porttitor lectus elementum magna odio aliquam odio aliquam montes et integer ut dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(7,'field',23,'en_us',' ultricies sociis pulvinar montes adipiscing nascetur porttitor ut elementum magna elit duis etiam ridiculus massa dolor sit nascetur urna hac ac integer ultricies ultrices lectus sit turpis vel et placerat integer penatibus tincidunt elit mus enim ac urna cursus porttitor lectus elementum magna odio aliquam odio aliquam montes et integer ut dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(7,'slug',0,'en_us',''),
	(8,'field',24,'en_us',' dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(8,'field',25,'en_us',' dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(8,'field',26,'en_us',' dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(8,'slug',0,'en_us',''),
	(9,'field',24,'en_us',' dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(9,'field',25,'en_us',' dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(9,'field',26,'en_us',' dis placerat dolor a sagittis eu integer nec ultrices etiam sit purus ut diam sed et sed odio elementum pid pulvinar eu proin non et etiam a pid augue mauris in vel in egestas habitasse natoque auctor porta elit nascetur amet magnis urna enim purus ridiculus massa sed et natoque '),
	(9,'slug',0,'en_us',''),
	(10,'field',28,'en_us',' screen shot 2016 12 19 at 4 40 10 pm screen shot 2016 12 20 at 12 31 37 pm screen shot 2016 12 20 at 9 42 04 am '),
	(10,'slug',0,'en_us',''),
	(11,'filename',0,'en_us',' screen shot 2016 12 20 at 12 31 37 pm png '),
	(11,'extension',0,'en_us',' png '),
	(11,'kind',0,'en_us',' image '),
	(11,'slug',0,'en_us',' screen shot 2016 12 20 at 12 31 37 pm '),
	(11,'title',0,'en_us',' screen shot 2016 12 20 at 12 31 37 pm '),
	(12,'filename',0,'en_us',' screen shot 2016 12 20 at 9 42 04 am png '),
	(12,'extension',0,'en_us',' png '),
	(12,'kind',0,'en_us',' image '),
	(12,'slug',0,'en_us',' screen shot 2016 12 20 at 9 42 04 am '),
	(12,'title',0,'en_us',' screen shot 2016 12 20 at 9 42 04 am '),
	(13,'filename',0,'en_us',' screen shot 2016 12 19 at 4 40 10 pm png '),
	(13,'extension',0,'en_us',' png '),
	(13,'kind',0,'en_us',' image '),
	(13,'slug',0,'en_us',' screen shot 2016 12 19 at 4 40 10 pm '),
	(13,'title',0,'en_us',' screen shot 2016 12 19 at 4 40 10 pm '),
	(2,'field',29,'en_us',''),
	(16,'field',32,'en_us',' screen shot 2016 12 20 at 12 31 37 pm '),
	(15,'field',30,'en_us',' facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue ac eu auctor phasellus rhoncus mattis nascetur aenean etiam elit nisi odio in odio amet mid odio magnis mus turpis lacus non duis proin magna tincidunt turpis velit mus augue augue aenean arcu urna sociis pulvinar habitasse est penatibus adipiscing mauris tincidunt turpis odio amet sociis purus placerat diam nunc adipiscing ultricies auctor odio turpis vel mauris lectus velit sit tincidunt turpis nunc dapibus facilisis a enim cursus ridiculus lacus cursus lorem nec nisi mus augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed sed elementum ultricies scelerisque rhoncus ultrices massa augue lorem augue egestas urna dis tristique etiam porta etiam mattis eros duis '),
	(15,'field',31,'en_us',' facilisis etiam porta mattis augue hac montes habitasse et placerat arcu augue amet augue ac eu auctor phasellus rhoncus mattis nascetur aenean etiam elit nisi odio in odio amet mid odio magnis mus turpis lacus non duis proin magna tincidunt turpis velit mus augue augue aenean arcu urna sociis pulvinar habitasse est penatibus adipiscing mauris tincidunt turpis odio amet sociis purus placerat diam nunc adipiscing ultricies auctor odio turpis vel mauris lectus velit sit tincidunt turpis nunc dapibus facilisis a enim cursus ridiculus lacus cursus lorem nec nisi mus augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed sed elementum ultricies scelerisque rhoncus ultrices massa augue lorem augue egestas urna dis tristique etiam porta etiam mattis eros duis '),
	(15,'slug',0,'en_us',''),
	(16,'field',33,'en_us',' cursus lorem nec nisi mus augue lorem sagittis habitasse egestas mid egestas in nisi nunc sed sed elementum ultricies scelerisque rhoncus ultrices massa augue lorem augue egestas urna dis tristique etiam porta etiam mattis eros duis '),
	(16,'field',34,'en_us',' test byline '),
	(16,'slug',0,'en_us',''),
	(31,'field',43,'en_us',' lkajdsf alkdsjfl kajsd fkl '),
	(31,'field',42,'en_us',' test 2 '),
	(30,'slug',0,'en_us',''),
	(30,'field',43,'en_us',' lkajdsf ksdf kal dfja sdl kjfal ksdjf  '),
	(28,'field',39,'en_us',' this is a link '),
	(28,'slug',0,'en_us',''),
	(29,'slug',0,'en_us',''),
	(30,'field',42,'en_us',' test 1 '),
	(28,'field',38,'en_us',' we just installed craft '),
	(27,'slug',0,'en_us',''),
	(27,'field',37,'en_us',' screen shot 2016 12 19 at 4 40 10 pm screen shot 2016 12 20 at 12 31 37 pm screen shot 2016 12 20 at 9 42 04 am '),
	(31,'slug',0,'en_us','');

/*!40000 ALTER TABLE `craft_searchindex` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sections`;

CREATE TABLE `craft_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('single','channel','structure') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'channel',
  `hasUrls` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enableVersioning` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_sections_name_unq_idx` (`name`),
  UNIQUE KEY `craft_sections_handle_unq_idx` (`handle`),
  KEY `craft_sections_structureId_fk` (`structureId`),
  CONSTRAINT `craft_sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_sections` WRITE;
/*!40000 ALTER TABLE `craft_sections` DISABLE KEYS */;

INSERT INTO `craft_sections` (`id`, `structureId`, `name`, `handle`, `type`, `hasUrls`, `template`, `enableVersioning`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,'Homepage','homepage','single',1,'index',1,'2016-12-20 19:47:28','2016-12-20 19:47:28','83f38eb9-1ebe-4f75-9cb6-70475f746437'),
	(2,NULL,'News','news','channel',1,'news/_entry',1,'2016-12-20 19:47:28','2016-12-20 19:47:28','a71b87bb-6b2a-445f-b914-2bb1f3ca810d'),
	(3,NULL,'Landing Page','landingPage','channel',1,'landing-page/_entry',1,'2017-01-03 05:14:29','2017-01-03 05:14:29','53522208-3fa4-4b60-a445-af12d5d0d90e');

/*!40000 ALTER TABLE `craft_sections` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_sections_i18n
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sections_i18n`;

CREATE TABLE `craft_sections_i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `enabledByDefault` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_sections_i18n_sectionId_locale_unq_idx` (`sectionId`,`locale`),
  KEY `craft_sections_i18n_locale_fk` (`locale`),
  CONSTRAINT `craft_sections_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_sections_i18n_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_sections_i18n` WRITE;
/*!40000 ALTER TABLE `craft_sections_i18n` DISABLE KEYS */;

INSERT INTO `craft_sections_i18n` (`id`, `sectionId`, `locale`, `enabledByDefault`, `urlFormat`, `nestedUrlFormat`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'en_us',1,'__home__',NULL,'2016-12-20 19:47:28','2016-12-20 19:47:28','cde28200-39d6-4620-9030-5d007d1db12c'),
	(2,2,'en_us',1,'news/{postDate.year}/{slug}',NULL,'2016-12-20 19:47:28','2016-12-20 19:47:28','0b3fcca6-3e56-402c-a318-dc43b108e229'),
	(3,3,'en_us',1,'landing-page/{slug}',NULL,'2017-01-03 05:14:29','2017-01-03 05:14:29','4530abf5-97fc-48d9-bece-3c46b40c3777');

/*!40000 ALTER TABLE `craft_sections_i18n` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sessions`;

CREATE TABLE `craft_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `token` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_sessions_uid_idx` (`uid`),
  KEY `craft_sessions_token_idx` (`token`),
  KEY `craft_sessions_dateUpdated_idx` (`dateUpdated`),
  KEY `craft_sessions_userId_fk` (`userId`),
  CONSTRAINT `craft_sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_sessions` WRITE;
/*!40000 ALTER TABLE `craft_sessions` DISABLE KEYS */;

INSERT INTO `craft_sessions` (`id`, `userId`, `token`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'a77119edd880a7aa3e42e92653b3198ed5468bddczozMjoiMFlVVFpUODVBUnRrUE9VMmh3MlYxZW1hRkFJSmFOYk0iOw==','2016-12-20 19:47:28','2016-12-20 19:47:28','d00ae958-6cc8-4200-a445-b7604dfe07ce'),
	(2,1,'1789adf44d66af9e0dfdd9f9a4bd36c55723bcfdczozMjoieF9ra3lzSnU1MjZ3aGxDVUZIeWZsOFBYTHpBQV9fYTgiOw==','2017-01-03 02:02:54','2017-01-03 02:02:54','9c8c8e77-90bb-4397-badb-bfd179d8cdf4'),
	(3,1,'1a8a27e1b153cc31f06f064ed2bbdff762abf2e5czozMjoiT1FRZnJyVlVoR35fQnQ2WFR2cU9ONU9hUWZUcGVjOU4iOw==','2017-01-03 03:46:47','2017-01-03 03:46:47','d93f6921-1523-4e24-8285-6303a87bc4be');

/*!40000 ALTER TABLE `craft_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_shunnedmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_shunnedmessages`;

CREATE TABLE `craft_shunnedmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_shunnedmessages_userId_message_unq_idx` (`userId`,`message`),
  CONSTRAINT `craft_shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_structureelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_structureelements`;

CREATE TABLE `craft_structureelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  KEY `craft_structureelements_root_idx` (`root`),
  KEY `craft_structureelements_lft_idx` (`lft`),
  KEY `craft_structureelements_rgt_idx` (`rgt`),
  KEY `craft_structureelements_level_idx` (`level`),
  KEY `craft_structureelements_elementId_fk` (`elementId`),
  CONSTRAINT `craft_structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_structureelements` WRITE;
/*!40000 ALTER TABLE `craft_structureelements` DISABLE KEYS */;

INSERT INTO `craft_structureelements` (`id`, `structureId`, `elementId`, `root`, `lft`, `rgt`, `level`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(63,14,NULL,63,1,16,0,'2017-01-03 05:32:15','2017-01-03 05:32:15','d1256240-f1ac-440d-8a01-38ddd4a3102c'),
	(64,14,15,63,2,3,1,'2017-01-03 05:32:15','2017-01-03 05:32:15','0bc39bac-1f57-4d29-b854-2815511c3c10'),
	(65,14,27,63,4,5,1,'2017-01-03 05:32:15','2017-01-03 05:32:15','d7efc734-e6cf-4290-bcde-c5254a21089d'),
	(66,14,16,63,6,7,1,'2017-01-03 05:32:15','2017-01-03 05:32:15','70d16a5d-b87c-4cb0-a8a9-277fd4bb354f'),
	(67,14,28,63,8,9,1,'2017-01-03 05:32:15','2017-01-03 05:32:15','baaa16c8-79c1-4807-94c1-b7fb1182d27a'),
	(68,14,29,63,10,15,1,'2017-01-03 05:32:15','2017-01-03 05:32:15','0531f46e-2cf2-417c-8638-5b5ac0d538d8'),
	(69,14,30,63,11,12,2,'2017-01-03 05:32:15','2017-01-03 05:32:15','229eab6f-4e9e-46f4-85ba-1a03ee70b4ea'),
	(70,14,31,63,13,14,2,'2017-01-03 05:32:15','2017-01-03 05:32:15','585e6db3-0d71-416c-bac8-81e4b5943c48');

/*!40000 ALTER TABLE `craft_structureelements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_structures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_structures`;

CREATE TABLE `craft_structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxLevels` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_structures` WRITE;
/*!40000 ALTER TABLE `craft_structures` DISABLE KEYS */;

INSERT INTO `craft_structures` (`id`, `maxLevels`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(14,NULL,'2017-01-03 05:32:15','2017-01-03 05:32:15','991ece30-0daf-4190-9613-a919df5da932');

/*!40000 ALTER TABLE `craft_structures` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_systemsettings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_systemsettings`;

CREATE TABLE `craft_systemsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_systemsettings_category_unq_idx` (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_systemsettings` WRITE;
/*!40000 ALTER TABLE `craft_systemsettings` DISABLE KEYS */;

INSERT INTO `craft_systemsettings` (`id`, `category`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'email','{\"protocol\":\"smtp\",\"emailAddress\":\"justin.lobaito@weloideas.com\",\"senderName\":\"Local Craft\",\"smtpAuth\":1,\"username\":\"jplobaito\",\"password\":\"Jpl@57006\",\"smtpSecureTransportType\":\"none\",\"port\":\"587\",\"host\":\"smtp.sendgrid.net\",\"timeout\":\"30\"}','2016-12-20 19:47:28','2016-12-20 20:26:19','97c3fee5-6e74-48b9-84fe-6428aa548cd7');

/*!40000 ALTER TABLE `craft_systemsettings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_taggroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_taggroups`;

CREATE TABLE `craft_taggroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_taggroups_name_unq_idx` (`name`),
  UNIQUE KEY `craft_taggroups_handle_unq_idx` (`handle`),
  KEY `craft_taggroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_taggroups` WRITE;
/*!40000 ALTER TABLE `craft_taggroups` DISABLE KEYS */;

INSERT INTO `craft_taggroups` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Default','default',1,'2016-12-20 19:47:28','2016-12-20 19:47:28','452d6bfe-6158-40a9-8ed1-f1b0bdd13d41');

/*!40000 ALTER TABLE `craft_taggroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_tags`;

CREATE TABLE `craft_tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_tags_groupId_fk` (`groupId`),
  CONSTRAINT `craft_tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_taggroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_tags_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_tasks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_tasks`;

CREATE TABLE `craft_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `currentStep` int(11) unsigned DEFAULT NULL,
  `totalSteps` int(11) unsigned DEFAULT NULL,
  `status` enum('pending','error','running') COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settings` mediumtext COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_tasks_root_idx` (`root`),
  KEY `craft_tasks_lft_idx` (`lft`),
  KEY `craft_tasks_rgt_idx` (`rgt`),
  KEY `craft_tasks_level_idx` (`level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_templatecachecriteria
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_templatecachecriteria`;

CREATE TABLE `craft_templatecachecriteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `criteria` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `craft_templatecachecriteria_cacheId_fk` (`cacheId`),
  KEY `craft_templatecachecriteria_type_idx` (`type`),
  CONSTRAINT `craft_templatecachecriteria_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `craft_templatecaches` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_templatecacheelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_templatecacheelements`;

CREATE TABLE `craft_templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  KEY `craft_templatecacheelements_cacheId_fk` (`cacheId`),
  KEY `craft_templatecacheelements_elementId_fk` (`elementId`),
  CONSTRAINT `craft_templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `craft_templatecaches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_templatecaches
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_templatecaches`;

CREATE TABLE `craft_templatecaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `craft_templatecaches_expiryDate_cacheKey_locale_path_idx` (`expiryDate`,`cacheKey`,`locale`,`path`),
  KEY `craft_templatecaches_locale_fk` (`locale`),
  CONSTRAINT `craft_templatecaches_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_tokens`;

CREATE TABLE `craft_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `route` text COLLATE utf8_unicode_ci,
  `usageLimit` tinyint(3) unsigned DEFAULT NULL,
  `usageCount` tinyint(3) unsigned DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_tokens_token_unq_idx` (`token`),
  KEY `craft_tokens_expiryDate_idx` (`expiryDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_usergroups`;

CREATE TABLE `craft_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_usergroups_name_unq_idx` (`name`),
  UNIQUE KEY `craft_usergroups_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_usergroups_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_usergroups_users`;

CREATE TABLE `craft_usergroups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  KEY `craft_usergroups_users_userId_fk` (`userId`),
  CONSTRAINT `craft_usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_userpermissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpermissions`;

CREATE TABLE `craft_userpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_userpermissions_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_userpermissions_usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpermissions_usergroups`;

CREATE TABLE `craft_userpermissions_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  KEY `craft_userpermissions_usergroups_groupId_fk` (`groupId`),
  CONSTRAINT `craft_userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `craft_userpermissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_userpermissions_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpermissions_users`;

CREATE TABLE `craft_userpermissions_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  KEY `craft_userpermissions_users_userId_fk` (`userId`),
  CONSTRAINT `craft_userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `craft_userpermissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table craft_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_users`;

CREATE TABLE `craft_users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preferredLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weekStartDay` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `client` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `suspended` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pending` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `archived` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIPAddress` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(4) unsigned DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `verificationCode` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwordResetRequired` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_users_username_unq_idx` (`username`),
  UNIQUE KEY `craft_users_email_unq_idx` (`email`),
  KEY `craft_users_verificationCode_idx` (`verificationCode`),
  KEY `craft_users_uid_idx` (`uid`),
  KEY `craft_users_preferredLocale_fk` (`preferredLocale`),
  CONSTRAINT `craft_users_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_users_preferredLocale_fk` FOREIGN KEY (`preferredLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_users` WRITE;
/*!40000 ALTER TABLE `craft_users` DISABLE KEYS */;

INSERT INTO `craft_users` (`id`, `username`, `photo`, `firstName`, `lastName`, `email`, `password`, `preferredLocale`, `weekStartDay`, `admin`, `client`, `locked`, `suspended`, `pending`, `archived`, `lastLoginDate`, `lastLoginAttemptIPAddress`, `invalidLoginWindowStart`, `invalidLoginCount`, `lastInvalidLoginDate`, `lockoutDate`, `verificationCode`, `verificationCodeIssuedDate`, `unverifiedEmail`, `passwordResetRequired`, `lastPasswordChangeDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'admin',NULL,NULL,NULL,'justin.lobaito@weloideas.com','$2y$13$A//zMb6mPWbLo0zyZZdFO.bLd4vzFCNn7hz9maXE93hKGygt78Ffm',NULL,0,1,0,0,0,0,0,'2017-01-03 03:46:47','127.0.0.1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2016-12-20 19:47:26','2016-12-20 19:47:26','2017-01-03 03:46:47','68e51f4c-fb0d-4062-b0df-afb60d8386a5');

/*!40000 ALTER TABLE `craft_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_widgets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_widgets`;

CREATE TABLE `craft_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `colspan` tinyint(4) unsigned DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_widgets_userId_fk` (`userId`),
  CONSTRAINT `craft_widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `craft_widgets` WRITE;
/*!40000 ALTER TABLE `craft_widgets` DISABLE KEYS */;

INSERT INTO `craft_widgets` (`id`, `userId`, `type`, `sortOrder`, `colspan`, `settings`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'RecentEntries',1,NULL,NULL,1,'2016-12-20 19:47:30','2016-12-20 19:47:30','04511b62-40c3-4541-b5e8-caf7f7fe21bd'),
	(2,1,'GetHelp',2,NULL,NULL,1,'2016-12-20 19:47:30','2016-12-20 19:47:30','03564b4c-52c0-4258-be0f-4733a21bd2ef'),
	(3,1,'Updates',3,NULL,NULL,1,'2016-12-20 19:47:30','2016-12-20 19:47:30','7a426a67-cd08-4b9a-8a4c-64d42d438709'),
	(4,1,'Feed',4,NULL,'{\"url\":\"https:\\/\\/craftcms.com\\/news.rss\",\"title\":\"Craft News\"}',1,'2016-12-20 19:47:30','2016-12-20 19:47:30','623a6510-c0cd-4e0b-a62a-7e76500178c7');

/*!40000 ALTER TABLE `craft_widgets` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
