<?php
namespace Craft;

/**
 * Lead service.
 *
 * Provides an API for the plugin to access the database.
 *
 * @package Craft
 *
 */
class LeadsService extends BaseApplicationComponent
{
    /**
     * Save a new lead.
     *
     * @param LeadsModel $lead
     * @throws Exception
     *
     */
    public function saveLead(LeadsModel $lead)
    {
        $settings = craft()->plugins->getPlugin('leads')->getSettings();

        if (!$settings->toEmail) {
            throw new Exception('The "To Email" address is not set on the plugin\'s settings page.');
        }

        $record = new LeadsRecord();
        $record->setAttributes($lead->getAttributes());

        if ($record->save()) {
            $toEmails = ArrayHelper::stringToArray($settings->toEmail);

            /**
             * Send an email to each address listed
             * in the plugin settings.
             */
            foreach ($toEmails as $toEmail) {
                $emailSettings = craft()->email->getSettings();

                $email = new EmailModel();

                $email->fromEmail = $emailSettings['emailAddress'];
                $email->replyTo = $lead->email;
                $email->sender = $emailSettings['emailAddress'];
                $email->fromName = $lead->name;
                $email->toEmail = $toEmail;
                $email->subject = $settings->adminSubject;

                $emailMessage = '<p>Hi there.<br>';
                $emailMessage .= 'There has been a new lead form submission!</p>';
                $emailMessage .= '<p><strong>Name:</strong> ' . $lead->name . '<br>';
                $emailMessage .= '<strong>Email:</strong> ' . $lead->email . '<br>';
                $emailMessage .= '<strong>Buyer/Seller:</strong> ' . $lead->status;
                $emailMessage .= '</p>';

                if ($lead->comment != '') {
                    $emailMessage .= '<p><strong>Comments:</strong><br>';
                    $emailMessage .= $lead->comment;
                    $emailMessage .= '</p>';
                } else {
                    $emailMessage .= '<p>Bummer, it looks like they didn\'t want to leave a message.</p>';
                }

                $emailMessage .= '<p><small>P.S. You can find all of this information in the control panel as well.</small></p>';

                $email->body = $emailMessage;

                craft()->email->sendEmail($email);
            }

            /**
             * Send an email to the user.
             *
             */
            $email = new EmailModel();

            $email->fromEmail = $emailSettings['emailAddress'];
            $email->replyTo = 'noreply@charterhouseiowa.com';
            $email->sender = $emailSettings['emailAddress'];
            $email->fromName = $emailSettings['senderName'];
            $email->toEmail = $lead->email;
            $email->subject = $settings->guestSubject;
            $email->body = craft()->templates->renderString($settings->welcomeEmailMessage, array('name' => $lead->name));

            if (craft()->email->sendEmail($email)) {
                return true;
            }

        } else {
            $lead->addErrors($record->getErrors());

            return false;
        }
    }

    /**
     * Return a list of all leads.
     *
     * @return array|\CDbDataReader
     *
     */
    public function getLeads()
    {
        return LeadsRecord::model()->findAll(array('order' => 'dateCreated desc'));
    }

    /**
     * Delete the given lead.
     *
     * @param $leadId
     * @return bool
     *
     */
    public function deleteLead($leadId)
    {
        return LeadsRecord::model()->deleteByPk($leadId);
    }

    /**
     * Get any given lead by Id.
     * @param {string} $leadId | Lead ID.
     * @return {object}
     *
     */
    public function getLead($leadId)
    {
        return LeadsRecord::model()->findByPk($leadId);
    }
}