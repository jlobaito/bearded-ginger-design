<?php
namespace Craft;

/**
 * Lead model.
 *
 * Provides a read-only data object representing a contact.
 * Data is returned by the service.
 *
 * @package Craft
 *
 */
class LeadsModel extends BaseModel
{
    /**
     * Define the data attributes for each record (row in the database).
     *
     * @return array
     *
     */
    public function defineAttributes()
    {
        return array(
            'name' => array(
                'type' => AttributeType::Name,
                'required' => true,
                'maxLength' => 100,
            ),
            'email' => array(
                'type' => AttributeType::Email,
                'required' => true,
                'maxLength' => 100,
            ),
            'status' => array(
                'type' => AttributeType::Name,
                'required' => true,
                'maxLength' => 20,
            ),
            'comment' => array(
                'type' => AttributeType::Template
            )
        );
    }
}