<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 **/

$url =  "http://{$_SERVER['HTTP_HOST']}";

return array(
    '*' => array(
        'omitScriptNameInUrls' => true,
        'enableCsrfProtection' => true,
        'csrfTokenName' => 'CSRF',
        'siteUrl' => $url,
        'environmentVariables' => array(
            'basePath' => $_SERVER['DOCUMENT_ROOT'] . '/',
            'baseUrl' => $url .'/',
        )
    ),
    '.dev' => array(
        'devMode' => true,
    ),
    'beardedgingerdesigns.com' => array(
        'devMode' => true,

    )
);
